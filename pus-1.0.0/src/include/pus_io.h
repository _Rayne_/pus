/**
 * @file pus_io.h
 *
 * @brief Brief description.
 *
 * Created by Rayne on 20.06.2014
 * Version 1.0.0.0
 */

#ifndef PUS_IO_H_
#define PUS_IO_H_

/***************************** Include Files *********************************/
#include <xil_types.h>

/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
u8* PUS_Read16(u8* addr, u16* value);

u8* PUS_Write16(u8* addr, u16 value);

u8* PUS_Read32(u8* addr, u32* value);

u8* PUS_Write32(u8* addr, u32 value);

u8* PUS_Read16S(u8* addr, s16* value);

u8* PUS_Read32S(u8* addr, s32* value);

u8* PUS_Write32S(u8* addr, s32 value);

#endif /* PUS_IO_H_ */

/*
 * storage_retrieval.h
 *
 * Defines storage and retrieval service. Service support only one
 * packet store. Retrieved packets are downlinked directly into a
 * dedicated virtual channel, i.e. exactly as received by the packet store.
 *
 *  Created on: 15.04.2014
 *      Author: Rayne
 */

#ifndef STORAGE_H_
#define STORAGE_H_

/***************************** Include Files *********************************/
#include <xil_types.h>
#include "pus.h"
#include "pus_config.h"

/************************** Constant Definitions *****************************/
#define STORAGE_SIZE					1000	///< Storage size in bytes (Should be synchronized with file svc).

/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
void Storage_Input(u8* data, int len);
void Storage_DownlinkAllHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer);
void Storage_DownlinkTimePeriodHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer);

#endif /* STORAGE_H_ */

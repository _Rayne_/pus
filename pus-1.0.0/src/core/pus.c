/**
 * @file pus.c
 *
 * @brief Implements PUS v.0 protocol.
 *
 *  Created on: 05.12.2013
 *      Author: Rayne
 * */

/***************************** Include Files *********************************/

#include "pus.h"
#include <string.h>
#include <stddef.h>
//#include <xil_printf.h>

/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/

/***************** Macros (Inline Functions) Definitions *********************/


/************************** Variable Definitions *****************************/
//static PUS_TCPacketHeader PUS_HeaderRx;
//static PUS_TMPacketHeader PUS_HeaderTx;
static struct PUS_PCB* pus_pcb_list = NULL;

static u8* currentTxBuffPtr = NULL;

/************************** Function Prototypes ******************************/
//int PUSAPP_GetCurrentRxSAP(PUS_ServiceProvider* instancePtr);

/************************ Function Implementations ***************************/
struct PUS_PCB* PUS_PCBNew()
{
	struct PUS_PCB * pcb = (struct PUS_PCB *)malloc(sizeof(struct PUS_PCB));
	/* could allocate UDP PCB? */
	if (pcb != NULL) {
		memset(pcb, 0, sizeof(struct PUS_PCB));
	}
	return pcb;
}

void PUS_Add(struct PUS_PCB* pcb, u16 apid, u8 svcType, u8 svcSubType, PUS_ReceiveHandler handler)
{
	//TODO: add duplication search.
	pcb->APID = apid;
	pcb->ServiceType = svcType;
	pcb->ServiceSubType = svcSubType;
	pcb->RecvHandler = handler;

	pcb->Next = pus_pcb_list;
	pus_pcb_list = pcb;
}

void PUS_Input(u8* buffer, const struct SP_PCB* sp_pcb)
{
	/* Check SP status */

	PUS_PacketHeaderBase pus_header = *((PUS_PacketHeaderBase*)buffer);

	struct PUS_PCB* pcb = pus_pcb_list;
	while(pcb != NULL)
	{
		if(SP_PACKET_ID_APID_CMP(pcb->APID, SP_CurrentHeader.PacketId) &&
				pcb->ServiceType == pus_header.ServiceType &&
				pcb->ServiceSubType == pus_header.ServiceSubType)
			break;
		pcb = pcb->Next;
	}

	if(pcb == NULL)
	{
		PUS_DEBUG_PRINT("PUS : No handler for (%d, %d)\r\n", pus_header.ServiceType, pus_header.ServiceSubType);
//		ACK_SendFailure(pcb, SP_CurrentHeader, pcb->PUS_TCHeader, 0, 0);
		return;
	}

	int secHeaderSize = 0;
	if(SP_PACKET_ID_GET_TYPE(SP_CurrentHeader.PacketId) == 0)
	{
		/* telemetry packet received */
		secHeaderSize = sizeof(PUS_TMPacketHeader);
		memcpy(&(pcb->PUS_TMHeader), buffer, secHeaderSize);
	}else
	{
		/* telecommand packet received */
		secHeaderSize = sizeof(PUS_TCPacketHeader);
		memcpy(&(pcb->PUS_TCHeader), buffer, secHeaderSize);
	}

	if(pcb->RecvHandler == NULL)
	{
		PUS_DEBUG_PRINT("PUS : invalid handler for (%d, %d)\r\n", pus_header.ServiceType, pus_header.ServiceSubType);
//		ACK_SendFailure();
		return;
	}

//	ACK_SendAccept();
	pcb->RecvHandler(pcb, sp_pcb, buffer + secHeaderSize);
}

u8* PUS_PacketOpenCommand(u16 destApid, u8 destSvcType, u8 destSvcSubType, u8 ack, u8 sourceId)
{
	u8* buff = SP_PacketOpen(destApid, SP_PACKET_ID_TC_TYPE, TRUE, StandAlone);

	if(buff == NULL)
	{
		PUS_DEBUG_PRINT("PUS : Could not open command packet\r\n");
		return NULL;
	}

	PUS_TCPacketHeader pus_tc_header;
	PUS_HEADER_BASE_TC_SET_CCSDSSHF(pus_tc_header.Base.FlagVerACK, TRUE);
	PUS_HEADER_BASE_SET_VERSION(pus_tc_header.Base.FlagVerACK, PUS_VERSION);
	PUS_HEADER_BASE_TC_SET_ACK(pus_tc_header.Base.FlagVerACK, ack);
	pus_tc_header.Base.ServiceType = destSvcType;
	pus_tc_header.Base.ServiceSubType = destSvcSubType;
	pus_tc_header.SourceId = sourceId;

	memcpy(buff, &pus_tc_header, PUS_TC_HEADER_SIZE);
	currentTxBuffPtr = buff + PUS_TC_HEADER_SIZE;
	return currentTxBuffPtr;
}

u8* PUS_PacketOpenTelemetry(u16 srcApid, u8 srcSvcType, u8 srcSvcSubType)
{
	struct PUS_PCB* pcb = pus_pcb_list;
	while(pcb != NULL)
	{
		if(SP_PACKET_ID_APID_CMP(pcb->APID, srcApid) &&
				pcb->ServiceType == srcSvcType &&
				pcb->ServiceSubType == srcSvcSubType)
			break;
		pcb = pcb->Next;
	}

	if(pcb == NULL)
	{
		pcb = PUS_PCBNew();
		PUS_Add(pcb, srcApid, srcSvcType, srcSvcSubType, NULL);
		PUS_DEBUG_PRINT("PUS : New pcb added (%d, %d)\r\n", srcSvcType, srcSvcSubType);
	}

	(pcb->Counter)++;

	u8* buff = SP_PacketOpen(srcApid, SP_PACKET_ID_TM_TYPE, TRUE, StandAlone);

	if(buff == NULL)
	{
		PUS_DEBUG_PRINT("PUS : Could not open telemetry packet\r\n");
		return NULL;
	}

	PUS_TMPacketHeader pus_tm_header;
	PUS_HEADER_BASE_SET_VERSION(pus_tm_header.Base.FlagVerACK, PUS_VERSION);
	pus_tm_header.Base.ServiceType = srcSvcType;
	pus_tm_header.Base.ServiceSubType = srcSvcSubType;
	pus_tm_header.Counter = pcb->Counter;

	//TODO: add time source.
	pus_tm_header.Time = 0;

	memcpy(buff, &pus_tm_header, PUS_TM_HEADER_SIZE);
	currentTxBuffPtr = buff + PUS_TM_HEADER_SIZE;
	return currentTxBuffPtr;
}

void PUS_PacketRelease(int appDataLen)
{
	SP_PacketSetSize(PUS_TM_HEADER_SIZE + appDataLen + PUS_TM_CHECKSUM_SIZE - 1);
	SP_AppendPacketCS(-PUS_TM_CHECKSUM_SIZE);

	int pSize = SP_HEADER_SIZE_BYTES + PUS_TM_HEADER_SIZE + appDataLen;

	Storage_SelectionInput(TxBuffer, pSize);

	SP_Release();
}

///**
// * @brief Reads PUS telecomand packet header and changes current provider state.
// *
// * @param[in, out] instancePtr Pus provider ptr.
// * @param[in] pdu Protocol data unit buffer ptr.
// * @param[in] pdu_start protocol data unit offset.
// * @param[out] sap Service that was activated.
// * @return New cursor position.
// */
//u8* PUS_TCRead(PUS_ServiceProvider* instancePtr, u8* pdu, int pdu_start, int* sap)
//{
//	memcpy(&(instancePtr->PUS_HeaderRx), pdu + pdu_start, sizeof(PUS_TCPacketHeader));
//
//	*sap = PUSAPP_GetCurrentRxSAP(instancePtr);
//	if(*sap != -1)
//		instancePtr->LoockupTable[*sap].Count++;
//	else
//		Register_WriteCF("ERROR", "PUS:No SAP found, ST %d SST %d\r\n",
//				instancePtr->PUS_HeaderRx.Base.ServiceType, instancePtr->PUS_HeaderRx.Base.ServiceSubType);
//
//	return pdu + pdu_start + sizeof(PUS_TCPacketHeader);
//}
//
///**
// * @brief Writes telemetry packet header, changes currents provider state.
// *
// * @param instancePtr Provider ptr.
// * @param sap Service access point that was activated.
// * @param pdu Protocol data unit buffer.
// * @param pdu_start Buffer offset.
// * @return Buffer cursir position.
// */
//u8* PUS_TMWrite(PUS_ServiceProvider* instancePtr, int sap, u8* pdu, int pdu_start)
//{
//	//TODO: Add time fixation.
//	instancePtr->PUS_HeaderTx.Base = instancePtr->LoockupTable[sap].HeaderBase;
//	instancePtr->PUS_HeaderTx.Counter = ++(instancePtr->LoockupTable[sap].Count);
//
//	PUS_PrintHeader(instancePtr->PUS_HeaderTx.Base);
//
//	memcpy(pdu + pdu_start, &(instancePtr->PUS_HeaderTx), sizeof(PUS_TMPacketHeader));
//	return pdu + pdu_start + sizeof(PUS_TMPacketHeader);
//}
//
///**
// * @brief Writes telemetry packet checksum.
// *
// * @param instancePtr Provider ptr.
// * @param pdu Protocol data unit buffer.
// * @param pdu_start Buffer offset.
// * @param position Current cursor position.
// * @return New cursor position.
// */
//u8* PUS_TMWriteChecksum(PUS_ServiceProvider* instancePtr, u8* pdu, int pdu_start, u8* position)
//{
//	//TODO: Add checksum calculation.
//	return position + 2;
//}
//
///**
// * @brief Checks presents of specified flag in telecommand ACK field.
// *
// * @param instancePtr Provider ptr.
// * @param flag Flag to check.
// * @return >0 - if flag presents and 0 othervise.
// */
//u8 PUS_CheckTCPCUACK(PUS_ServiceProvider* instancePtr, u8 flag)
//{
//	return PUS_HEADER_BASE_TC_GET_ACK(instancePtr->PUS_HeaderRx.Base.FlagVerACK) & flag;
//}
//
///**
// * @brief Returns active providers service access point
// *
// * @param instancePtr Provider ptr.
// * @return SAP.
// */
//int PUSAPP_GetCurrentRxSAP(PUS_ServiceProvider* instancePtr)
//{
//	int i = 0;
//	for (i = 0; i < PUS_SERVICE_LOOCKUP_TABLE_SIZE; ++i) {
//		if(memcmp(&(instancePtr->LoockupTable[i].HeaderBase.ServiceType),
//				&(instancePtr->PUS_HeaderRx.Base.ServiceType), 2) == 0)
//		{
//			return i;
//		}
//	}
//	return -1;
//}

void PUS_PrintHeader(PUS_PacketHeaderBase header)
{
#if defined(_PUS_DEBUG_)
	xil_printf("----------------------- PUS packet header ---------\r\n");
	xil_printf("| SHF/SP | Version | ACK | SVC Type | SVC SubType | \r\n");
	xil_printf("|      %x |       %x |   %x |      %03.x |         %03.x | \r\n",
			PUS_HEADER_BASE_TC_GET_CCSDSSHF(header.FlagVerACK),
			PUS_HEADER_BASE_GET_VERSION(header.FlagVerACK),
			PUS_HEADER_BASE_TC_GET_ACK(header.FlagVerACK),
			header.ServiceType,
			header.ServiceSubType);
	xil_printf("---------------------------------------------------\r\n");
#endif
}

/*
 * verification.h
 *
 *  Created on: 12.12.2013
 *      Author: Rayne
 */

#ifndef VERIFICATION_H_
#define VERIFICATION_H_

/***************************** Include Files *********************************/
#include <xil_types.h>

/************************** Constant Definitions *****************************/

/**************************** Type Definitions *******************************/
typedef enum{
	VFC_ILLEGAL_APID = 0,
	VFC_INCOMPLETE_OR_INVALID_LENGTH = 1,
	VFC_INCCORRECT_CHECKSUM = 2,
	VFC_ILLEGAL_PACKET_TYPE = 3,
	VFC_ILLEGAL_PACKET_SUBTYPE = 4,
	VFC_INCOMPLETE_OR_INCONSISTENT_ADD_DATA = 5,
	VFC_APPLICATION_ERROR = 6
}Verify_FailureCode;

/***************** Macros (Inline Functions) Definitions *********************/
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
u8* Verify_Accept(u16 packetId, u16 sequenceCtrl, u8* pdu, int start);

u8* Verify_Failure(u16 packetId, u16 sequenceCtrl, Verify_FailureCode code, u8 param, u8* pdu, int start);

u8* Verify_ExecutionStart(u16 packetId, u16 sequenceCtrl, u8* pdu, int start);

u8* Verify_ExecutionStartFailure(u16 packetId, u16 sequenceCtrl, u8 code, u8 param, u8* pdu, int start);

u8* Verify_ExecutionProcess(u16 packetId, u16 sequenceCtrl, u16 step, u8* pdu, int start);

u8* Verify_ExecutionProcessFailure(u16 packetId, u16 sequenceCtrl, u16 step, u8 code, u8 param, u8* pdu, int start);

u8* Verify_ExecutionComplete(u16 packetId, u16 sequenceCtrl, u8* pdu, int start);

u8* Verify_ExecutionCompleteFailure(u16 packetId, u16 sequenceCtrl, u8 code, u8 param, u8* pdu, int start);

#endif /* VERIFICATION_H_ */

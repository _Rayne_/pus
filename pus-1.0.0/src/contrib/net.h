/**
 * @file net.h
 *
 * @brief Network interface declaration.
 *
 * Implement this function to provide access to network.
 *
 * Created by Rayne on 04.09.2014
 * Version 1.0.0.0
 */

#ifndef NET_H_
#define NET_H_

/***************************** Include Files *********************************/
#include "udp_svc_send.h"
/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
#define NET_PACKET_OUT(psasp, pdsap, channel, data, len)			SP_SendCore(psasp, pdsap, channel, data, len)
//int SP_SendCore(u32 psasp, u32 pdsap, u8 channel, u8* data, int len);
#endif /* NET_H_ */

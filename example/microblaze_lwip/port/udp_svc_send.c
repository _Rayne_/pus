/**
 * @file udp_svc_send.c
 *
 * @brief Brief description.
 *
 * Created by Rayne on 06.06.2014
 * Version 1.0.0.0
 */

/***************************** Include Files *********************************/
#include "space_packet.h"
#include "../src/udp_svc.h"

/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
#define _IP4_ADDR(a,b,c,d) \
        ((u32_t)((d) & 0xff) << 24) | \
        ((u32_t)((c) & 0xff) << 16) | \
        ((u32_t)((b) & 0xff) << 8)  | \
        (u32_t)((a) & 0xff)

/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
/************************ Function Implementations ***************************/
int SP_SendCore(u32 psasp, u32 pdsap, u8 channel, u8* data, int len)
{
	/* get route */
	struct ip_addr ip = {_IP4_ADDR(192,168,231,238)};
	udp_svc_send_to(&ip, 501, data, len);
	return 0;
}


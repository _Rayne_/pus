/*
 * test.c
 *
 *  Created on: 23.01.2014
 *      Author: Rayne
 */

/***************************** Include Files *********************************/
#include "test.h"
#include "ack.h"
//#include "event_reporting.h"

/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
int Test_SendConnectionTestReport(u16 arcApid);

/************************ Function Implementations ***************************/
void Test_ConnectionHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer)
{
	int status = PUS_STATUS_OK;

	status = ACK_SendAccept(SP_CurrentHeader, pus_pcb->PUS_TCHeader);
	if(status != PUS_STATUS_OK)
		return;

	status = ACK_SendExecutionStart(SP_CurrentHeader, pus_pcb->PUS_TCHeader);
	if(status != PUS_STATUS_OK)
		return;

	status = ACK_SendExecutionProcess(SP_CurrentHeader, pus_pcb->PUS_TCHeader, 0);
	if(status != PUS_STATUS_OK)
		return;

	status = Test_SendConnectionTestReport(SP_PACKET_ID_GET_APID(SP_CurrentHeader.PacketId));
	if(status != PUS_STATUS_OK)
		return;

	ACK_SendExecutionComplete(SP_CurrentHeader, pus_pcb->PUS_TCHeader);
}

/**
 * @brief Sends connection test report.
 *
 * @return Send status.
 * 	@arg #PUSAPP_ERROR - Application general error.
 * 	@arg #PUSAPP_SP_PROTOCOL_ERROR - Space packet protocol error.
 * 	@arg #PUSAPP_PACKET_SVC_ERROR - Packet service access error.
 * 	@arg #PUSAPP_OK - Send success.
 */
int Test_SendConnectionTestReport(u16 srcApid)
{
	PUS_PacketOpenTelemetry(srcApid, 17, 2);
	PUS_PacketRelease(0);
	return PUS_STATUS_OK;
}


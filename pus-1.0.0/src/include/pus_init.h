/**
 * @file pus_init.h
 *
 * @brief Brief description.
 *
 * Created by Rayne on 15.09.2014
 * Version 1.0.0.0
 */

#ifndef INIT_H_
#define INIT_H_

/***************************** Include Files *********************************/
/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
void PUS_Init();

#endif /* INIT_H_ */

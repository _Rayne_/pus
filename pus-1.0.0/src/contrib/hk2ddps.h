/**
 * @file hk2ddps.h
 *
 * @brief Maps HK service to DDPS.
 *
 * Created by Rayne on 03.09.2014
 * Version 1.0.0.0
 */

#ifndef HK2DDPS_H_
#define HK2DDPS_H_

/***************************** Include Files *********************************/
/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
typedef void HK2DDPS_OrderType;

/***************** Macros (Inline Functions) Definitions *********************/
#define HK2DDPS_DATA_POOL_SAMPLE_PARAMETERS_SIZE_WORDS(orderPtr)		0

/* C usage syntax int HK_DDPS_READ_LAST_SAMPLE(, u16*) */
#define HK2DDPS_READ_LAST_SAMPLE(orderPtr, buffer)			0

#define HK2DDPS_DISABLE_REPORT(sid)
#define HK2DDPS_ENABLE_REPORT(sid)
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/


#endif /* HK2DDPS_H_ */

/**
 * @file dcd2das.h
 *
 * @brief Mapping dcd to das.
 *
 * Created by Rayne on 03.09.2014
 * Version 1.0.0.0
 */

#ifndef DCD2DAS_MAP_H_
#define DCD2DAS_MAP_H_

/***************************** Include Files *********************************/
/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
//TODO: You should implement device access logic
#define DCD_COMMAND_STATUS_OK				0		///< Status from DAS service

/* C usage syntax
 * int DCD_COMMAND(u16 device_id, u16 value_id, void* cmd_buf, int cmd_buf_len, void* value_buf, int value_buf_len, int* value_len)
 */
#define DCD_COMMAND(device_id, value_id, cmd_buf, cmd_buf_len, value_buf, value_buf_len, value_len)		DCD_COMMAND_STATUS_OK


#endif /* DCD2DAS_MAP_H_ */

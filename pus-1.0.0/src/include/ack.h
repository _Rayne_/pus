/**
 * @file ack.h
 *
 * @brief Defines acknowledgment api.
 *
 * Created by Rayne on 05.06.2014
 * Version 1.0.0.0
 */

#ifndef ACK_H_
#define ACK_H_

/***************************** Include Files *********************************/
#include "pus.h"
#include "verification.h"

/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
int ACK_SendAccept(SP_PacketHeader procPktSPHeader, PUS_TCPacketHeader procPktPUSHeader);
int ACK_SendFailure(SP_PacketHeader procPktSPHeader, PUS_TCPacketHeader procPktPUSHeader, Verify_FailureCode code, u8 param);
int ACK_SendExecutionStart(SP_PacketHeader procPktSPHeader, PUS_TCPacketHeader procPktPUSHeader);
int ACK_SendExecutionStartFailure(SP_PacketHeader procPktSPHeader, PUS_TCPacketHeader procPktPUSHeader, u8 code, u8 param);
int ACK_SendExecutionProcess(SP_PacketHeader procPktSPHeader, PUS_TCPacketHeader procPktPUSHeader, u16 step);
int ACK_SendExecutionProcessFailure(SP_PacketHeader procPktSPHeader, PUS_TCPacketHeader procPktPUSHeader, u16 step, u8 code, u8 param);
int ACK_SendExecutionComplete(SP_PacketHeader procPktSPHeader, PUS_TCPacketHeader procPktPUSHeader);
int ACK_SendExecutionCompleteFailure(SP_PacketHeader procPktSPHeader, PUS_TCPacketHeader procPktPUSHeader, u8 code, u8 param);

#endif /* ACK_H_ */

/*
 * event_reporting.c
 *
 *  Created on: 24.01.2014
 *      Author: Rayne
 */

/***************************** Include Files *********************************/
#include "space_packet.h"
#include "pus.h"
#include "ack.h"
#include "pus_io.h"
#include "event_reporting.h"

/************************** Constant Definitions *****************************/


#define EVENT_FIFO_SIZE								5
/**************************** Type Definitions *******************************/
typedef int (*send_event_handler)(u16 src_apid, u32 rid, u32 parameter);

typedef struct {
	u32 rid;
	u32 parameter;
	u16 apid;
	send_event_handler handler;
}Event_FifoEntry;

/***************** Macros (Inline Functions) Definitions *********************/
/************************** Variable Definitions *****************************/
extern u32 Reports[];

Event_FifoEntry fifo[EVENT_FIFO_SIZE] = {{0}};
int fifo_end = 0;
int fifo_begin = 0;

/************************** Function Prototypes ******************************/
/************************ Function Implementations ***************************/
/**
 * @brief Reports normal/progress event.
 *
 * @return Send status.
 * 	@arg #PUSAPP_ERROR - Application general error.
 * 	@arg #PUSAPP_SP_PROTOCOL_ERROR - Space packet protocol error.
 * 	@arg #PUSAPP_PACKET_SVC_ERROR - Packet service access error.
 * 	@arg #PUS_STATUS_OK - Send success.
 */
int Event_SendNormalReport(u16 src_apid, u32 rid, u32 parameter)
{
	if(rid >= EVENT_REPORTS_TABLE_SIZE)
		return PUS_STATUS_INVALID_EVENT_ID;

	if(!Reports[rid])
		return PUS_STATUS_OK;

	u8* buffer = PUS_PacketOpenTelemetry(src_apid, 5, 1);
	u8* begin = buffer;

	buffer = PUS_Write32(buffer, rid);
	buffer = PUS_Write32(buffer, parameter);

	PUS_PacketRelease(buffer - begin);

	return PUS_STATUS_OK;
}

/**
 * @brief Reports error/anomaly event of low severity.
 *
 * @return Send status.
 * 	@arg #PUSAPP_ERROR - Application general error.
 * 	@arg #PUSAPP_SP_PROTOCOL_ERROR - Space packet protocol error.
 * 	@arg #PUSAPP_PACKET_SVC_ERROR - Packet service access error.
 * 	@arg #PUS_STATUS_OK - Send success.
 */
int Event_SendErrorAnomalyLowReport(u16 src_apid, u32 rid, u32 parameter)
{
	if(rid >= EVENT_REPORTS_TABLE_SIZE)
		return PUS_STATUS_INVALID_EVENT_ID;

	if(!Reports[rid])
		return PUS_STATUS_OK;

	u8* buffer = PUS_PacketOpenTelemetry(src_apid, 5, 2);
	u8* begin = buffer;

	buffer = PUS_Write32(buffer, rid);
	buffer = PUS_Write32(buffer, parameter);

	PUS_PacketRelease(buffer - begin);
	return PUS_STATUS_OK;
}

/**
 * @brief Reports error/anomaly of medium severity.
 *
 * @return Send status.
 * 	@arg #PUSAPP_ERROR - Application general error.
 * 	@arg #PUSAPP_SP_PROTOCOL_ERROR - Space packet protocol error.
 * 	@arg #PUSAPP_PACKET_SVC_ERROR - Packet service access error.
 * 	@arg #PUS_STATUS_OK - Send success.
 */
int Event_SendErrorAnomalyMediumReport(u16 src_apid, u32 rid, u32 parameter)
{
	if(rid >= EVENT_REPORTS_TABLE_SIZE)
		return PUS_STATUS_INVALID_EVENT_ID;

	if(!Reports[rid])
		return PUS_STATUS_OK;

	u8* buffer = PUS_PacketOpenTelemetry(src_apid, 5, 3);
	u8* begin = buffer;

	buffer = PUS_Write32(buffer, rid);
	buffer = PUS_Write32(buffer, parameter);

	PUS_PacketRelease(buffer - begin);

	return PUS_STATUS_OK;
}

/**
 * @brief Reports error/anomaly of high severity.
 *
 * @return Send status.
 * 	@arg #PUSAPP_ERROR - Application general error.
 * 	@arg #PUSAPP_SP_PROTOCOL_ERROR - Space packet protocol error.
 * 	@arg #PUSAPP_PACKET_SVC_ERROR - Packet service access error.
 * 	@arg #PUS_STATUS_OK - Send success.
 */
int Event_SendErrorAnomalyHighReport(u16 src_apid, u32 rid, u32 parameter)
{
	if(rid >= EVENT_REPORTS_TABLE_SIZE)
		return PUS_STATUS_INVALID_EVENT_ID;

	if(!Reports[rid])
		return PUS_STATUS_OK;

	u8* buffer = PUS_PacketOpenTelemetry(src_apid, 5, 4);
	u8* begin = buffer;

	buffer = PUS_Write32(buffer, rid);
	buffer = PUS_Write32(buffer, parameter);

	PUS_PacketRelease(buffer - begin);
	return PUS_STATUS_OK;
}

/**
 * @brief Handles enable event report telecommand packet.
 *
 * Enables specified event reporting for #SP_MEASURING_APID.
 *
 * @note In general reports table should be defined for each apid.
 *
 * @param packet_id Telecommand packet id.
 * @param sequence_ctrl Telecommand sequence controller.
 * @param buffer Telecommand packet buffer source.
 * @param start Telecommand packet buffer offset.
 */
void Event_EnableHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer)
{
	ACK_SendAccept(SP_CurrentHeader, pus_pcb->PUS_TCHeader);

	u16 nrid = 0;
	buffer = PUS_Read16(buffer, &nrid);

	if (nrid > EVENT_REPORTS_TABLE_SIZE) {
		ACK_SendExecutionStartFailure(SP_CurrentHeader, pus_pcb->PUS_TCHeader, VFC_INCOMPLETE_OR_INCONSISTENT_ADD_DATA, 0);
		return;
	}

	int i = 0;
	u16 rid = 0;
	for (i = 0; i < nrid; ++i) {
		buffer = PUS_Read16(buffer, &rid);

		//The rid number should be less than table size cause it is using like index.
		if (rid >= EVENT_REPORTS_TABLE_SIZE) {
			ACK_SendExecutionProcessFailure(SP_CurrentHeader, pus_pcb->PUS_TCHeader, i, VFC_INCOMPLETE_OR_INCONSISTENT_ADD_DATA, 0);
			return;
		}

		Reports[rid] = TRUE;

		ACK_SendExecutionProcess(SP_CurrentHeader, pus_pcb->PUS_TCHeader, i);
	}

	ACK_SendExecutionComplete(SP_CurrentHeader, pus_pcb->PUS_TCHeader);
}

/**
 * @brief Handles disable event report telecommand packet.
 *
 * Disables specified event reporting for #SP_MEASURING_APID.
 *
 * @note In general reports table should be defined for each apid.
 *
 * @param packet_id Telecommand packet id.
 * @param sequence_ctrl Telecommand sequence controller.
 * @param buffer Telecommand packet buffer source.
 * @param start Telecommand packet buffer offset.
 */
void Event_DisableHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer)
{
	ACK_SendAccept(SP_CurrentHeader, pus_pcb->PUS_TCHeader);

	u16 nrid = 0;
	buffer = PUS_Read16(buffer, &nrid);

	if (nrid > EVENT_REPORTS_TABLE_SIZE) {
		ACK_SendExecutionStartFailure(SP_CurrentHeader, pus_pcb->PUS_TCHeader, VFC_INCOMPLETE_OR_INCONSISTENT_ADD_DATA, 0);
		return;
	}

	int i = 0;
	u16 rid = 0;
	for (i = 0; i < nrid; ++i) {
		buffer = PUS_Read16(buffer, &rid);

		//The rid number should be less than table size cause it is using like index.
		if (rid >= EVENT_REPORTS_TABLE_SIZE) {
			ACK_SendExecutionProcessFailure(SP_CurrentHeader, pus_pcb->PUS_TCHeader, i, VFC_INCOMPLETE_OR_INCONSISTENT_ADD_DATA, 0);
			return;
		}

		Reports[rid] = FALSE;

		ACK_SendExecutionProcess(SP_CurrentHeader, pus_pcb->PUS_TCHeader, i);
	}

	ACK_SendExecutionComplete(SP_CurrentHeader, pus_pcb->PUS_TCHeader);
}

/**
 * @brief Event service heartbeat.
 *
 * Function for processing asynchrous event messages coming through interrupts in master thread.
 *
 * @note Function uses thread blocking.
 */
void Event_SyncHeartBeat()
{
	while(fifo_begin != fifo_end)
	{
		if(PUS_STATUS_OK != fifo[fifo_begin].handler(fifo[fifo_begin].apid ,fifo[fifo_begin].rid, fifo[fifo_begin].parameter))
			PUS_DEBUG_PRINT("Events report error\r\n");

		fifo_begin++;
		if(fifo_begin >= EVENT_FIFO_SIZE)
			fifo_begin = 0;
	}
}

void increment_fifo()
{
	fifo_end++;
	if(fifo_end >= EVENT_FIFO_SIZE)
		fifo_end = 0;

	if(fifo_end == fifo_begin)
		PUS_DEBUG_PRINT("Events fifo overflow\r\n");
}

/**
 * @brief Safe call of normal event report generation.
 *
 * Use this function to call event report generation from interrupt handlers.
 * Actual report generation going in master thread.
 *
 * @param rid Report Id.
 * @param parameter Parameter.
 */
void Event_SendNormalReportSafe(u16 src_apid, u32 rid, u32 parameter)
{
	fifo[fifo_end].rid = rid;
	fifo[fifo_end].parameter = parameter;
	fifo[fifo_end].handler = Event_SendNormalReport;
	increment_fifo();
}

/**
 * @brief Safe call of error\anomaly low event report generation.
 *
 * Use this function to call event report generation from interrupt handlers.
 * Actual report generation going in master thread.
 *
 * @param rid Report Id.
 * @param parameter Parameter.
 */
void Event_SendErrorAnomalyLowReportSafe(u16 src_apid, u32 rid, u32 parameter)
{
	fifo[fifo_end].rid = rid;
	fifo[fifo_end].parameter = parameter;
	fifo[fifo_end].handler = Event_SendErrorAnomalyLowReport;
	increment_fifo();
}

/**
 * @brief Safe call of error\anomaly medium event report generation.
 *
 * Use this function to call event report generation from interrupt handlers.
 * Actual report generation going in master thread.
 *
 * @param rid Report Id.
 * @param parameter Parameter.
 */
void Event_SendErrorAnomalyMediumReportSafe(u16 src_apid, u32 rid, u32 parameter)
{
	fifo[fifo_end].rid = rid;
	fifo[fifo_end].parameter = parameter;
	fifo[fifo_end].handler = Event_SendErrorAnomalyMediumReport;
	increment_fifo();
}

/**
 * @brief Safe call of error\anomaly high event report generation.
 *
 * Use this function to call event report generation from interrupt handlers.
 * Actual report generation going in master thread.
 *
 * @param rid Report Id.
 * @param parameter Parameter.
 */
void Event_SendErrorAnomalyHighReportSafe(u16 src_apid, u32 rid, u32 parameter)
{
	fifo[fifo_end].rid = rid;
	fifo[fifo_end].parameter = parameter;
	fifo[fifo_end].handler = Event_SendErrorAnomalyHighReport;
	increment_fifo();
}

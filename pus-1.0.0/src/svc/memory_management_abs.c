/**
 * @file memory_management_abs.c
 *
 * @brief Brief description.
 *
 * Created by Rayne on 15.09.2014
 * Version 1.0.0.0
 */

/***************************** Include Files *********************************/
#include <stdlib.h>
#include <string.h>
#include "memory_management.h"
#include "ack.h"
#include "pus_io.h"

#include "../contrib/mm2fs.h"

/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
/**
 * @name Memory management svc basic io.
 *
 * @{
 *  */
static inline u16 Memory_GetCheckSum(MM_SAU* data_ptr, size_t units)
{
	return SP_Fletcher16((u8*)data_ptr, MM_SDU_TO_BYTES(units));
}

inline u8* Memory_WriteAbsoluteBlock(Memory_Block block, u8* buffer)
{
	buffer = PUS_Write32(buffer, block.Field1.StartAddr);

	return PUS_Write32(buffer, block.Length);
}

inline u8* Memory_ReadAbsoluteBlock(u8* buffer, int start, Memory_Block* block)
{
	buffer += start;

	buffer = PUS_Read32(buffer, &(block->Field1.StartAddr));

	return PUS_Read32(buffer, &(block->Length));
}

/**
 * @brief Reads absolute operations type header.
 *
 * @param[in] buffer Buffer to read from.
 * @param[out] name The name of file or memory is being processed.
 * @param[out] n_ptr The number of memory blocks to process.
 * @return The pointer to position right after header.
 */
inline u8* Memory_ReadAbsoluteHeader(u8* buffer, char* name, u32* n_ptr)
{
	memcpy(name, buffer, MM_MEMORY_NAME_LENGTH);
	buffer += MM_MEMORY_NAME_LENGTH;

	buffer = PUS_Read32(buffer, n_ptr);
	return buffer;
}

/**
 * @brief Writes absolute operations type header.
 *
 * @param[in] buffer Buffer to read from.
 * @param[in] name The name of file or memory is being processed.
 * @param[in] n The number of memory blocks to process.
 * @return The pointer to position right after header.
 */
inline u8* Memory_WriteAbsoluteHeader(u8* buffer, const char* name, u32 n)
{
	memcpy(buffer, name, MM_MEMORY_NAME_LENGTH);
	buffer += MM_MEMORY_NAME_LENGTH;

	buffer = PUS_Write32(buffer, n);
	return buffer;
}

inline u8* Memory_ReadAbsoluteDumpRequest(u8* buffer, char* name, u32* n_ptr, Memory_Block* blocks)
{
	buffer = Memory_ReadAbsoluteHeader(buffer, name, n_ptr);

	if(*n_ptr > MM_BLOCKS_MAX_NUM){
			return NULL;
	}

	int i = 0;
	for (i = 0; i < *n_ptr; ++i) {

		buffer = Memory_ReadAbsoluteBlock(buffer, 0, blocks + i);

		if (MM_SDU_TO_BYTES(blocks[i].Length) > MM_BLOCK_SIZE_MAX_BYTES) {
			/* rise execution starts failure and event */
			PUS_DEBUG_PRINT("MM : invalid length %d\r\n", blocks[i].Length);
			return NULL;
		}
	}

	return buffer;
}
//@}
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
/************************ Function Implementations ***************************/
static void Memory_BlockPrint(Memory_Block block)
{
	PUS_DEBUG_PRINT("MM : Block : %x, %x\r\n", block.Field1.Offset, block.Length);
}
/**
 * @brief Generates dump report packet and sends it to client.
 *
 * @param apid Source APID.
 * @param name File or memory name being processed.
 * @param base Memory base address in SAU.
 * @param n Number of memory blocks.
 * @param blocks Memory blocks array.
 * @return Send status.
 * 	@arg MM_OK
 * 	@arg MM_ERR
 * 	@arg MM_INVALID_BLOCKS_NUM
 * 	@arg MM_INVALID_FILE_MEMORY_NAME
 */
int Memory_SendDataAbsoluteBlockDump(u16 src_apid, const char* name, int n, Memory_Block* blocks)
{
	if(n <= 0 || n > MM_BLOCKS_MAX_NUM)
		return PUS_STATUS_MM_INVALID_BLOCKS_NUM;

	int m = MM_MEM_OPEN(name);
	if(m == -1)
	{
		//TODO: Generate event;
		return PUS_STATUS_MM_INVALID_MEMORY_NAME;
	}

	u8* buffer = PUS_PacketOpenTelemetry(src_apid, 6, 6);
	u8* begin = buffer;

	buffer = Memory_WriteAbsoluteHeader(buffer, name, n);
	if(buffer == NULL)
		return PUS_STATUS_ERROR;

	int wread = 0;
	u32 checksum = 0;
	int i = 0;
	for (i = 0; i < n; ++i) {
		buffer = Memory_WriteAbsoluteBlock(blocks[i], buffer);

		wread = MM_MEM_READ(m, 0, blocks[i].Field1.StartAddr, (u16*)buffer, MM_SDU_TO_16_WORDS(blocks[i].Length));

		checksum = Memory_GetCheckSum((MM_SAU*)buffer, blocks[i].Length);

		buffer += MM_SDU_TO_BYTES(blocks[i].Length);

		buffer = PUS_Write32(buffer, checksum);

		if(wread != MM_SDU_TO_16_WORDS(blocks[i].Length))
		{
			//TODO: Generate event;
		}
	}

	if(m != MM_MEM_CLOSE(m))
	{
		//TODO: Generate event.
	}

	PUS_PacketRelease(buffer - begin);

	return PUS_STATUS_OK;
}


/**
 * @brief Forms report packet and sends it to the client.
 *
 * @param apid Source APID.
 * @param name Memory or file name.
 * @param pus_sap Service access point.
 * 	@arg MEMORY_MANAGEMENT_BASE_OFFSET_CHECK_SAP
 * 	@arg MEMORY_MANAGEMENT_FILE_BASE_OFFSET_CHECK_SAP
 *
 * @param n Number of memory blocks.
 * @param blocks Memory blocks array.
 * @return Send status.
 * 	@arg MM_OK
 * 	@arg MM_ERR
 * 	@arg MM_INVALID_BLOCKS_NUM
 * 	@arg MM_INVALID_FILE_MEMORY_NAME
 */
int Memory_SendCheckDataAbsoluteReport(u16 src_apid, const char* name, int n, Memory_Block* blocks)
{
	if(n <= 0 || n > MM_BLOCKS_MAX_NUM)
		return PUS_STATUS_MM_INVALID_BLOCKS_NUM;

	int m_handle = MM_MEM_OPEN(name);
	if(m_handle == -1)
	{
		//TODO: Generate event;
		return PUS_STATUS_MM_INVALID_MEMORY_NAME;
	}

	u8* buffer = PUS_PacketOpenTelemetry(src_apid, 6, 8);
	u8* begin = buffer;

	buffer = Memory_WriteAbsoluteHeader(buffer, name, n);

	u16 checksum = 0;
	int i = 0;
	for (i = 0; i < n; ++i) {
		buffer = Memory_WriteAbsoluteBlock(blocks[i], buffer);

		checksum = MM_MEM_CHECK(m_handle, 0, blocks[i].Field1.StartAddr, MM_SDU_TO_BYTES(blocks[i].Length));

		buffer = PUS_Write16(buffer, checksum);
	}

	if(m_handle != MM_MEM_CLOSE(m_handle))
	{
		//TODO: Generate event.
	}

	PUS_PacketRelease(buffer - begin);

	return PUS_STATUS_OK;
}

void Memory_LoadDataAbsoluteHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer)
{
	char name[MM_MEMORY_NAME_LENGTH] = {0};
	u32 n = 0;

	buffer = Memory_ReadAbsoluteHeader(buffer, name, &n);

	if(n > MM_BLOCKS_MAX_NUM){
		ACK_SendFailure(SP_CurrentHeader, pus_pcb->PUS_TCHeader, VFC_INCOMPLETE_OR_INCONSISTENT_ADD_DATA, 0);
		return;
	}

	ACK_SendAccept(SP_CurrentHeader, pus_pcb->PUS_TCHeader);

	MM_SAU* data_ptr = NULL;
	u16 checksum = 0;

	Memory_Block block = {{0}};

	int i = 0;
	for (i = 0; i < n; ++i) {

		buffer = Memory_ReadAbsoluteBlock(buffer, 0, &block);

		Memory_BlockPrint(block);

		if (MM_SDU_TO_BYTES(block.Length) > MM_BLOCK_SIZE_MAX_BYTES) {
			/* rise execution process failure and event */
			ACK_SendExecutionProcessFailure(SP_CurrentHeader, pus_pcb->PUS_TCHeader, i, VFC_INCOMPLETE_OR_INCONSISTENT_ADD_DATA, 1);
			PUS_DEBUG_PRINT("MM : Block : invalid length %d\r\n", block.Length);
			return;
		}

		data_ptr = (MM_SAU*)buffer;
		buffer += MM_SDU_TO_BYTES(block.Length);
		buffer = PUS_Read16(buffer, &checksum);

		/* check entirety */
		if(checksum != Memory_GetCheckSum(data_ptr, block.Length))
		{
			ACK_SendExecutionProcessFailure(SP_CurrentHeader, pus_pcb->PUS_TCHeader, i, VFC_INCOMPLETE_OR_INCONSISTENT_ADD_DATA, 2);
			PUS_DEBUG_PRINT("MM : Block : invalid checksum %x\r\n", checksum);
			return;
		}

		/* call write */
		int m_handle = MM_MEM_OPEN(name);
		if(m_handle != -1){
			int wrote = MM_MEM_WRITE(m_handle, 0, block.Field1.StartAddr, data_ptr, MM_SDU_TO_BYTES(block.Length));
			if(m_handle != MM_MEM_CLOSE(m_handle))
			{
				//TODO: Generate event.
			}

			if (wrote == MM_SDU_TO_BYTES(block.Length)) {
				ACK_SendExecutionProcess(SP_CurrentHeader, pus_pcb->PUS_TCHeader, i);
			}else {
				ACK_SendExecutionProcessFailure(SP_CurrentHeader, pus_pcb->PUS_TCHeader, i, VFC_INCOMPLETE_OR_INCONSISTENT_ADD_DATA, 3);
				PUS_DEBUG_PRINT("MM : Block : Not all bytes wrote %x < %x\r\n", wrote, MM_SDU_TO_BYTES(block.Length));
				return;
			}
		}else
		{
			ACK_SendExecutionProcessFailure(SP_CurrentHeader, pus_pcb->PUS_TCHeader, i, VFC_INCOMPLETE_OR_INCONSISTENT_ADD_DATA, 4);
			PUS_DEBUG_PRINT("MM : Could not open file\r\n");
			return;
		}

		PUS_DEBUG_PRINT("\r\n");
	}

	ACK_SendExecutionComplete(SP_CurrentHeader, pus_pcb->PUS_TCHeader);
}

void Memory_DumpDataAbsoluteHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer)
{
	char name[MM_MEMORY_NAME_LENGTH] = {0};
	u32 n = 0;
	Memory_Block blocks[MM_BLOCKS_MAX_NUM] = {{0}};

	if(NULL == Memory_ReadAbsoluteDumpRequest(buffer, name, &n, blocks))
	{
		ACK_SendFailure(SP_CurrentHeader, pus_pcb->PUS_TCHeader, VFC_INCOMPLETE_OR_INCONSISTENT_ADD_DATA, 0);
		return;
	}

	ACK_SendAccept(SP_CurrentHeader, pus_pcb->PUS_TCHeader);

	/* call dump */

	//TODO: Add error notification.
	int status = Memory_SendDataAbsoluteBlockDump(pus_pcb->APID, name, n, blocks);
	if(PUS_STATUS_OK != status)
	{
		PUS_DEBUG_PRINT("MM : Send dump error %d\r\n", status);
		ACK_SendExecutionCompleteFailure(SP_CurrentHeader, pus_pcb->PUS_TCHeader, VFC_APPLICATION_ERROR, status);
	}
	else
		ACK_SendExecutionComplete(SP_CurrentHeader, pus_pcb->PUS_TCHeader);
}

void Memory_CheckDataAbsoluteHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer)
{
	char name[MM_MEMORY_NAME_LENGTH] = {0};
	u32 n = 0;
	Memory_Block blocks[MM_BLOCKS_MAX_NUM] = {{0}};

	if(NULL == Memory_ReadAbsoluteDumpRequest(buffer, name, &n, blocks))
	{
		ACK_SendFailure(SP_CurrentHeader, pus_pcb->PUS_TCHeader, VFC_INCOMPLETE_OR_INVALID_LENGTH, 0);
		return;
	}

	ACK_SendAccept(SP_CurrentHeader, pus_pcb->PUS_TCHeader);

	//TODO: Add error notification.
	int status = Memory_SendCheckDataAbsoluteReport(pus_pcb->APID, name, n, blocks);
	if(PUS_STATUS_OK != status)
	{
		PUS_DEBUG_PRINT("MM : Check sum error %d\r\n", status);
		ACK_SendExecutionCompleteFailure(SP_CurrentHeader, pus_pcb->PUS_TCHeader, VFC_APPLICATION_ERROR, status);
	}
	else
		ACK_SendExecutionComplete(SP_CurrentHeader, pus_pcb->PUS_TCHeader);
}

/*
 * event_reporting.h
 *
 *  Created on: 24.01.2014
 *      Author: Rayne
 */

#ifndef EVENT_REPORTING_H_
#define EVENT_REPORTING_H_

/***************************** Include Files *********************************/
#include <xil_types.h>
#include "pus_config.h"

/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
#define EVENT_REPORTS_TABLE_SIZE					40
#define EVENT_REPORTS_TABLE_END_MARKER				3

#define EVENT_REPORTS_TABLE_BEGIN()				u32 Reports[EVENT_REPORTS_TABLE_SIZE] = {
#define EVENT_REPORTS_TABLE_ENTRY(rid, enabled)		[rid] = enabled
#define EVENT_REPORTS_TABLE_END()					EVENT_REPORTS_TABLE_END_MARKER}
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
void Event_EnableHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer);
void Event_DisableHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer);

int Event_SendNormalReport(u16 src_apid, u32 rid, u32 parameter);
int Event_SendErrorAnomalyLowReport(u16 src_apid, u32 rid, u32 parameter);
int Event_SendErrorAnomalyMediumReport(u16 src_apid, u32 rid, u32 parameter);
int Event_SendErrorAnomalyHighReport(u16 src_apid, u32 rid, u32 parameter);

void Event_SyncHeartBeat();

void Event_SendNormalReportSafe(u16 src_apid, u32 rid, u32 parameter);
void Event_SendErrorAnomalyLowReportSafe(u16 src_apid, u32 rid, u32 parameter);
void Event_SendErrorAnomalyMediumReportSafe(u16 src_apid, u32 rid, u32 parameter);
void Event_SendErrorAnomalyHighReportSafe(u16 src_apid, u32 rid, u32 parameter);

#endif /* EVENT_REPORTING_H_ */

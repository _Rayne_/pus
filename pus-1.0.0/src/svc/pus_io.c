/**
 * @file pus_io.c
 *
 * @brief Brief description.
 *
 * Created by Rayne on 20.06.2014
 * Version 1.0.0.0
 */

/***************************** Include Files *********************************/
#include "pus_config.h"
#include "pus_io.h"
#include <string.h>

/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
/************************ Function Implementations ***************************/
u8* PUS_Read16(u8* addr, u16* value)
{
	memcpy(value, addr, 2);
	*value = PUS_PTOHS(*value);
	return addr + 2;
}

u8* PUS_Write16(u8* addr, u16 value)
{
	u16 val = PUS_HTOPS(value);
	memcpy(addr, &val, 2);
	return addr + 2;
}

u8* PUS_Read32(u8* addr, u32* value)
{
	memcpy(value, addr, 4);
	*value = PUS_PTOHL(*value);
	return addr + 4;
}

u8* PUS_Write32(u8* addr, u32 value)
{
	u32 val = PUS_HTOPL(value);
	memcpy(addr, &val, 4);
	return addr + 4;
}

u8* PUS_Read16S(u8* addr, s16* value)
{
	memcpy(value, addr, 2);
	*value = PUS_PTOHS(*value);
	return addr + 2;
}

u8* PUS_Read32S(u8* addr, s32* value)
{
	memcpy(value, addr, 4);
	*value = PUS_PTOHL(*value);
	return addr + 4;
}

u8* PUS_Write32S(u8* addr, s32 value)
{
	s32 val = PUS_HTOPL(value);
	memcpy(addr, &val, 4);
	return addr + 4;
}


/**
 * @file pus_init.c
 *
 * @brief Brief description.
 *
 * Created by Rayne on 15.09.2014
 * Version 1.0.0.0
 */

/***************************** Include Files *********************************/
#include "pus_init.h"
#include "pus_config.h"
#include "pus.h"

#include "test.h"
#include "memory_management.h"
#include "storage_retrieval.h"
#include "storage_selection.h"
#include "event_reporting.h"

/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
/************************ Function Implementations ***************************/
void PUS_Init()
{
	struct SP_PCB * sp = SP_PCBNew();
	SP_Add(sp, CURRENT_APID);

	struct PUS_PCB * pp = NULL;

	/* Event reporting service initialization (5) */
#if EVENT_REPORTIN_SVC & ADD_CAP
	pp = PUS_PCBNew();
	PUS_Add(pp, CURRENT_APID, 5, 5, Event_EnableHandler);
	pp = PUS_PCBNew();
	PUS_Add(pp, CURRENT_APID, 5, 6, Event_DisableHandler);
#endif

	/* Memory management initialization (6) */
#if MEMORY_MANAGEMENT_SVC & MIN_CAP
	pp = PUS_PCBNew();
	PUS_Add(pp, CURRENT_APID, 6, 1, Memory_LoadDataBaseOffsetHandler);
	pp = PUS_PCBNew();
	PUS_Add(pp, CURRENT_APID, 6, 2, Memory_LoadDataAbsoluteHandler);
	pp = PUS_PCBNew();
	PUS_Add(pp, CURRENT_APID, 6, 3, Memory_DumpDataBaseOffsetHandler);
	pp = PUS_PCBNew();
	PUS_Add(pp, CURRENT_APID, 6, 5, Memory_DumpDataAbsoluteHandler);
#endif
#if MEMORY_MANAGEMENT_SVC & ADD_CAP
	pp = PUS_PCBNew();
	PUS_Add(pp, CURRENT_APID, 6, 8, Memory_CheckDataBaseOffsetHandler);
	pp = PUS_PCBNew();
	PUS_Add(pp, CURRENT_APID, 6, 8, Memory_CheckDataAbsolutetHandler);
#endif

	/* storage and retrieval initialization (15) */
#if STORAGE_RETRIEVAL_SVC & MIN_CAP
	pp = PUS_PCBNew();
	PUS_Add(pp, CURRENT_APID, 15, 1, Storage_SelectionEnableHandler);
	pp = PUS_PCBNew();
	PUS_Add(pp, CURRENT_APID, 15, 2, Storage_SelectionDisableHandler);

	pp = PUS_PCBNew();
	PUS_Add(pp, CURRENT_APID, 15, 7, Storage_DownlinkAllHandler);
#endif

	/* Test service initialization (17) */
#if TEST_SVC & MIN_CAP
	pp = PUS_PCBNew();
	PUS_Add(pp, CURRENT_APID, 17,1, Test_ConnectionHandler);
#endif
}



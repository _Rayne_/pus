/**
 * @file device_command_distribution.c
 *
 * @brief Brief description.
 *
 * Created by Rayne on 27.05.2014
 * Version 1.0.0.0
 */

/***************************** Include Files *********************************/
#include <stdlib.h>
#include <string.h>
#include "space_packet.h"
#include "pus.h"
#include "ack.h"
#include "pus_io.h"
#include "device_command_distribution.h"

#include "../contrib/dcd2das.h"

/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/

/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
/************************ Function Implementations ***************************/
/**
 * @brief Distributes command for specified device
 */
void Device_DistributeCommandHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer)
{
	int status = PUS_STATUS_OK;

	status = ACK_SendExecutionStart(SP_CurrentHeader, pus_pcb->PUS_TCHeader);
	if(status != PUS_STATUS_OK)
		return;

	u16 N = 0;
	buffer = PUS_Read16(buffer, &N);

	int i = 0;
	for (i = 0; i < N; ++i) {
		u16 dev_id = 0, val_id = 0;
		u32 val = 0;
		buffer = PUS_Read16(buffer, &dev_id);
		buffer = PUS_Read16(buffer, &val_id);
		buffer = PUS_Read32(buffer, &val);

		int das_status = DCD_COMMAND(dev_id, val_id, NULL, 0, NULL, 0, NULL);
		if(das_status != DCD_COMMAND_STATUS_OK){
			status = ACK_SendExecutionProcessFailure(SP_CurrentHeader, pus_pcb->PUS_TCHeader, i, VFC_APPLICATION_ERROR, (u8)das_status);
			if(status != PUS_STATUS_OK)
				return;
		}

		status = ACK_SendExecutionProcess(SP_CurrentHeader, pus_pcb->PUS_TCHeader, i);
		if(status != PUS_STATUS_OK)
			return;
	}

	ACK_SendExecutionComplete(SP_CurrentHeader, pus_pcb->PUS_TCHeader);
}

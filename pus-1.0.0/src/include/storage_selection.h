/**
 * @file storage_selection.h
 *
 * @brief Brief description.
 *
 * Created by Rayne on 16.09.2014
 * Version 1.0.0.0
 */

#ifndef STORAGE_SELECTION_H_
#define STORAGE_SELECTION_H_

/***************************** Include Files *********************************/
#include <xil_types.h>
#include "pus.h"
#include "pus_config.h"

/************************** Constant Definitions *****************************/
#define STORAGE_FILTER_SIZE				10		///< Packet storage filter size.

/**************************** Type Definitions *******************************/
typedef struct {
	u16 APID;
	u8 Type;
	u8 SubType;
}Storage_PacketSelection;

/***************** Macros (Inline Functions) Definitions *********************/
/**
 * @name Packet storage filter definition macro
 * @{
 */
#define STORAGE_SELECTION_BEGIN()							Storage_PacketSelection StoragePacketsFilter[STORAGE_FILTER_SIZE] = {
#define STORAGE_SELECTION_ENTRY(apid, type, sub_type)		{.APID = (apid), .Type = (type), .SubType = (sub_type)}
#define STORAGE_SELECTION_END()								{.APID = 0, .Type = 0, .SubType = 0}}
//@}

/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
void Storage_SelectionEnableHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer);
void Storage_SelectionDisableHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer);

void Storage_SelectionInput(u8* data, int len);
void Storage_ReleasePackets(u16 src_apid);

#endif /* STORAGE_SELECTION_H_ */

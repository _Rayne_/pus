/**
 * @file space_packet.c
 *
 * @brief Space packet protocol v.0 implementation.
 *
 *  Created on: 21.11.2013
 *      Author: Rayne
 * */

/***************************** Include Files *********************************/
#include "pus.h"
#include <string.h>
#include <stdio.h>
#include "storage_selection.h"

/************************** Constant Definitions *****************************/


/**************************** Type Definitions *******************************/
typedef enum {
	PUS_TX_BUF_RELEASED = 0,
	PUS_TX_BUF_OPENED = 1
} TxBufferState;

/***************** Macros (Inline Functions) Definitions *********************/

/************************** Variable Definitions *****************************/
SP_PacketHeader SP_CurrentHeader;

static struct SP_PCB * pcb_list = NULL;

u8 TxBuffer[SP_MAX_LEN];
TxBufferState TxState = PUS_TX_BUF_RELEASED;


/************************** Function Prototypes ******************************/
int SP_GetSAP(struct SP_PCB* instancePtr);



void SP_CounterIncrement(u16* packet_sequence_control);

/************************ Function Implementations ***************************/
struct SP_PCB* SP_PCBNew()
{
	struct SP_PCB * pcb = NULL;
	  pcb = (struct SP_PCB *)malloc(sizeof(struct SP_PCB));
	  /* could allocate UDP PCB? */
	  if (pcb != NULL) {
	    memset(pcb, 0, sizeof(struct SP_PCB));
	  }
	  return pcb;
}

void SP_Add(struct SP_PCB* pcb, u16 apid)
{
	pcb->APID = apid;
//	SP_PACKET_ID_SET_APID(pcb->SP_RxHeader.PacketId, apid);
//	SP_PACKET_ID_SET_APID(pcb->SP_TxHeader.PacketId, apid);
	pcb->Next = pcb_list;
	pcb_list = pcb;
}

void SP_Input(u8* buffer)
{
	SP_PacketHeader header;
	SP_HeaderFromProtocolBytes(buffer, 0, &header);

	if(SP_PACKET_ID_GET_VERSION(header.PacketId) != SP_VERSION)
		return;

	struct SP_PCB* pcb = pcb_list;
	while(pcb != NULL)
	{
		if(SP_PACKET_ID_APID_CMP(pcb->APID, header.PacketId))
			break;
		pcb = pcb->Next;
	}

	/* return if no pcb found */
	if(pcb == NULL)
	{
		SP_DEBUG_PRINT("SP : no pcb for packet %x\r\n", header.PacketId);
		return;
	}

	SP_CurrentHeader = header;
	pcb->Status = SP_OK;
	/* Do some protocol error detection */
	/* Get previous count */
//	u16 prew_cnt = SP_PACKET_SEQ_CTRL_GET_CNT(SP_CurrentHeader.PacketSequenceControl);

	/* Generate warming if count was broken */
	if(SP_PACKET_SEQ_CTRL_GET_CNT(header.PacketSequenceControl) != ((pcb->InputCounter + 1) & 0x3FF))
	{
		pcb->Status = SP_SEQUENCE_BREAK;
		SP_DEBUG_PRINT("SP : packet %x sequence break\r\n", header.PacketId);
	}

//	if(SP_PACKET_SEQ_CTRL_GET_FLAG(instancePtr->SP_RxHeader.PacketSequenceControl) != StandAlone)
//	{
//		Register_WriteCF("WARNING", "SP:Only standalone supported, APID %d\r\n", SP_PACKET_ID_GET_APID(instancePtr->SP_RxHeader.PacketId));
//		instancePtr->Status = SP_SEQUENCE_ERROR;
//	}

	/* Save new count */
	pcb->InputCounter = header.PacketSequenceControl;

	/* Check packet max len */
	if(header.PacketLength > SP_MAX_LEN)
	{
		pcb->Status = SP_LENGTH_OVERFLOW;
		SP_DEBUG_PRINT("SP : packet %x length overflow(%d > %d), handling stopped\r\n",
				header.PacketId, header.PacketLength, SP_LENGTH_OVERFLOW);
		return;
	}

	PUS_Input(buffer + sizeof(SP_PacketHeader), pcb);
}

u8* SP_PacketOpen(u16 apid, u8 type, u8 secHeaderFlag, u8 seqFlag)
{
	if(TxState != PUS_TX_BUF_RELEASED)
		return NULL;

	//TODO: Lock the buffer;

	struct SP_PCB* pcb = pcb_list;
	while(pcb != NULL)
	{
		if(SP_PACKET_ID_APID_CMP(pcb->APID, apid))
			break;
		pcb = pcb->Next;
	}

	/* add new pcb if not found */
	if(pcb == NULL)
	{
		pcb = SP_PCBNew();
		SP_Add(pcb, apid);
		SP_DEBUG_PRINT("SP : New pcb added, apid :%d\r\n", apid);
	}

	TxState = PUS_TX_BUF_OPENED;

	SP_PacketHeader header;
	SP_PACKET_ID_SET_VERSION(header.PacketId, SP_VERSION);
	SP_PACKET_ID_SET_TYPE(header.PacketId, type);
	SP_PACKET_ID_SET_HEADER_FLAG(header.PacketId, secHeaderFlag);
	SP_PACKET_ID_SET_APID(header.PacketId, apid);

	SP_PACKET_SEQ_CTRL_SET_FLAG(header.PacketSequenceControl, seqFlag);
	SP_CounterIncrement(&(pcb->OutputCounter));
	SP_PACKET_SEQ_CTRL_SET_CNT(header.PacketSequenceControl, pcb->OutputCounter);

	header.PacketLength = 0; // Correct length before sending.
	SP_HeaderToProtocolBytes(&header, TxBuffer, 0);

	return TxBuffer + SP_HEADER_SIZE_BYTES;
}

void SP_PacketSetSize(u16 size)
{
	size = SP_HTOPS(size);
	memcpy(TxBuffer + offsetof(SP_PacketHeader, PacketLength), &size, 2);
}

u16 SP_PacketGetSize()
{
	u16 size = 0;
	memcpy(&size, TxBuffer + offsetof(SP_PacketHeader, PacketLength), 2);
	return SP_HTOPS(size);
}

/**
 * @brief Appends Fletcher16 to the end of packet on specified offset.
 *
 * Check sum is wrote in SP protocol bytes order (BE). Check sum
 * covers all packet bytes except itself.
 *
 * @param endOffset Offset to place CS.
 */
void SP_AppendPacketCS(int endOffset)
{
	int toCheck = SP_PacketGetSize() + 1 + SP_HEADER_SIZE_BYTES + endOffset;
	u16 cs = SP_Fletcher16(TxBuffer, toCheck);
	cs = SP_HTOPS(cs);
	memcpy(TxBuffer + toCheck, &cs, 2);
}

void SP_Release()
{
	int toSend = SP_PacketGetSize() + 1 + SP_HEADER_SIZE_BYTES;
//	SP_Otput(TxBuffer, toSend);

//	Storage_Input(TxBuffer, toSend);

	NET_PACKET_OUT(0, 0, 0, TxBuffer, toSend);

	TxState = PUS_TX_BUF_RELEASED;
}



u16 SP_Fletcher16( u8 const *data, size_t bytes )
{
	u16 sum1 = 0xff, sum2 = 0xff;

	while (bytes) {
		size_t tlen = bytes > 20 ? 20 : bytes;
		bytes -= tlen;
		do {
				sum2 += sum1 += *data++;
		} while (--tlen);
		sum1 = (sum1 & 0xff) + (sum1 >> 8);
		sum2 = (sum2 & 0xff) + (sum2 >> 8);
	}
	/* Second reduction step to reduce sums to 8 bits */
	sum1 = (sum1 & 0xff) + (sum1 >> 8);
	sum2 = (sum2 & 0xff) + (sum2 >> 8);
	return sum2 << 8 | sum1;
}

void SP_CounterIncrement(u16* packet_sequence_control)
{
	u16 cnt = SP_PACKET_SEQ_CTRL_GET_CNT(*packet_sequence_control);

	((cnt == SP_PACK_CNT_MAX_VAL) ? cnt = 0 : ++cnt);

	SP_PACKET_SEQ_CTRL_SET_CNT(*packet_sequence_control, cnt);
}

void SP_HeaderFromProtocolBytes(const u8* src, int start, SP_PacketHeader* header)
{
	header->PacketId = SP_PTOHS(*(u16*)(src + start));
	header->PacketSequenceControl = SP_PTOHS(*(u16*)(src + start + 2));
	header->PacketLength = SP_PTOHS(*(u16*)(src + start + 4));
}

void SP_HeaderToProtocolBytes(const SP_PacketHeader* header, u8* dst, int start)
{
	*(u16*)(dst + start) = SP_HTOPS(header->PacketId);
	*(u16*)(dst + start + 2) = SP_HTOPS(header->PacketSequenceControl);
	*(u16*)(dst + start + 4) = SP_HTOPS(header->PacketLength);
}
//
//void SP_Reset(struct SP_PCB* instancePtr)
//{
//	int i = 0;
//	for (i = 0; i < SP_LOOCKUP_TABLE_SIZE; ++i) {
//		SP_PACKET_SEQ_CTRL_SET_FLAG(instancePtr->SP_LoockupTable[i].PacketSequenceControl, StandAlone);
//		SP_PACKET_SEQ_CTRL_SET_CNT(instancePtr->SP_LoockupTable[i].PacketSequenceControl, SP_PACK_CNT_MAX_VAL);
//	}
//
////	SP_RxHeader.PacketId = 0;
////	SP_PACKET_ID_SET_VERSION(SP_RxHeader.PacketId, SP_VERSION);
////	SP_PACKET_ID_SET_APID(SP_RxHeader.PacketId, APID);
////	SP_PACKET_ID_SET_TYPE(SP_RxHeader.PacketId, 1);
////	SP_PACKET_ID_SET_HEADER_FLAG(SP_RxHeader.PacketId, 1);
////	SP_PACKET_SEQ_CTRL_SET_FLAG(SP_RxHeader.PacketSequenceControl, StandAlone);
////	SP_PACKET_SEQ_CTRL_SET_CNT(SP_RxHeader.PacketSequenceControl, SP_PACK_CNT_MAX_VAL);
////	SP_RxHeader.PacketLength = 0;
////
////	SP_TxHeader.PacketId = 0;
////	SP_PACKET_ID_SET_VERSION(SP_TxHeader.PacketId, SP_VERSION);
////	SP_PACKET_ID_SET_APID(SP_TxHeader.PacketId, APID);
////	SP_PACKET_ID_SET_TYPE(SP_TxHeader.PacketId, 0);
////	SP_PACKET_ID_SET_HEADER_FLAG(SP_TxHeader.PacketId, 1);
////	SP_PACKET_SEQ_CTRL_SET_FLAG(SP_TxHeader.PacketSequenceControl, StandAlone);
////	SP_PACKET_SEQ_CTRL_SET_CNT(SP_TxHeader.PacketSequenceControl, SP_PACK_CNT_MAX_VAL);
////	SP_TxHeader.PacketLength = 0;
//
//	instancePtr->Status = SP_OK;
//}
//
//u8* SP_HeaderWriteBegin(struct SP_PCB* instancePtr, int sap, u8* pdu, int pdu_start)
//{
//	if(sap <0 || sap >= SP_LOOCKUP_TABLE_SIZE)
//	{
//		SP_DEBUG_PRINT("ERROR:SP:invalid sap %d.\r\n", sap);
//		Register_WriteCF("ERROR", "SP:Invalid SAP\r\n", sap);
//		instancePtr->Status = SP_ERROR;
//		return pdu + pdu_start + sizeof(SP_PacketHeader);
//	}
//
//	SP_CounterIncrement(&(instancePtr->SP_LoockupTable[sap].PacketSequenceControl));
//
//	instancePtr->SP_TxHeader.PacketId = instancePtr->SP_LoockupTable[sap].PacketId;
//	instancePtr->SP_TxHeader.PacketSequenceControl = instancePtr->SP_LoockupTable[sap].PacketSequenceControl;
//	/* Use this field to check if this call was in the end of packet forming. */
//	instancePtr->SP_TxHeader.PacketLength = 0;
//
//	return pdu + pdu_start + sizeof(SP_PacketHeader);
//}
//
//int SP_HadetWriteEnd(struct SP_PCB* instancePtr, u8* pdu, int pdu_start, u16 data_field_length)
//{
//	if(instancePtr->SP_TxHeader.PacketLength != 0)
//	{
//		SP_DEBUG_PRINT("ERROR:SP:writing packet end before begin.\r\n");
//		Register_WriteCF("ERROR", "ERROR:SP:writing packet end before begin.\r\n");
//		instancePtr->Status = SP_ERROR;
//		//TODO: change
//		return 0;
//	}
//
//	instancePtr->SP_TxHeader.PacketLength = data_field_length - 1;
//
//	SP_PrintHeader(instancePtr->SP_TxHeader);
//
//	SP_HeaderToProtocolBytes(&(instancePtr->SP_TxHeader), pdu, pdu_start);
//
////	SP_PrintHeader((u8*)&SP_TxHeader);
//
//	return data_field_length + sizeof(SP_PacketHeader);
//}
//
//int SP_GetSAP(struct SP_PCB* instancePtr)
//{
//	int i = 0;
//	for (i = 0; i < SP_LOOCKUP_TABLE_SIZE; ++i) {
//		if(instancePtr->SP_LoockupTable[i].PacketId==instancePtr->SP_RxHeader.PacketId)
//		{
//			return i;
//		}
//	}
//	return -1;
//}

void SP_PrintHeader(SP_PacketHeader header)
{
#if defined(_SP_DEBUG_)
	xil_printf("------------------------ SP packet header ------------------\r\n");
	xil_printf("| Version | Type | HeaderF | APID | Flags | Count | Length | \r\n");
	xil_printf("|       %x |    %x |       %x | %04.x |     %x |  %04.x |  %05x | \r\n",
			SP_PACKET_ID_GET_VERSION(header.PacketId),
			SP_PACKET_ID_GET_TYPE(header.PacketId),
			SP_PACKET_ID_GET_HEADER_FLAG(header.PacketId),
			SP_PACKET_ID_GET_APID(header.PacketId),
			SP_PACKET_SEQ_CTRL_GET_FLAG(header.PacketSequenceControl),
			SP_PACKET_SEQ_CTRL_GET_CNT(header.PacketSequenceControl),
			header.PacketLength);
	xil_printf("------------------------------------------------------------\r\n");
#endif
}



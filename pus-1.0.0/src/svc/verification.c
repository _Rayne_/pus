/*
 * verification.c
 *
 *  Created on: 12.12.2013
 *      Author: Rayne
 */

/***************************** Include Files *********************************/
#include "verification.h"
#include <string.h>

/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
//#define VFC_HTOPS(val)		((val >> 8) | ((val << 8) & 0xFF00))
//#define VFC_PTOHS(val)		((val >> 8) | ((val << 8) & 0xFF00))

#define VFC_HTOPS(val)		(val)
#define VFC_PTOHS(val)		(val)


/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
/************************ Function Implementations ***************************/
u8* Verify_Accept(u16 packetId, u16 sequenceCtrl, u8* pdu, int start)
{
	memcpy(pdu + start, &(VFC_HTOPS(packetId)), 2);
	memcpy(pdu + start + 2, &(VFC_HTOPS(sequenceCtrl)), 2);
	return pdu + start + 4;
}

u8* Verify_Failure(u16 packetId, u16 sequenceCtrl, Verify_FailureCode code, u8 param, u8* pdu, int start)
{
	memcpy(pdu + start, &(VFC_HTOPS(packetId)), 2);
	memcpy(pdu + start + 2, &(VFC_HTOPS(sequenceCtrl)), 2);
	*(pdu + start + 4) = (u8)code;
	*(pdu + start + 5) = param;
	return pdu + start + 6;
}

u8* Verify_ExecutionStart(u16 packetId, u16 sequenceCtrl, u8* pdu, int start)
{
	memcpy(pdu + start, &(VFC_HTOPS(packetId)), 2);
	memcpy(pdu + start + 2, &(VFC_HTOPS(sequenceCtrl)), 2);
	return pdu + start + 4;
}

u8* Verify_ExecutionStartFailure(u16 packetId, u16 sequenceCtrl, u8 code, u8 param, u8* pdu, int start)
{
	memcpy(pdu + start, &(VFC_HTOPS(packetId)), 2);
	memcpy(pdu + start + 2, &(VFC_HTOPS(sequenceCtrl)), 2);
	*(pdu + start + 4) = code;
	*(pdu + start + 5) = param;
	return pdu + start + 6;
}

u8* Verify_ExecutionProcess(u16 packetId, u16 sequenceCtrl, u16 step, u8* pdu, int start)
{
	memcpy(pdu + start, &(VFC_HTOPS(packetId)), 2);
	memcpy(pdu + start + 2, &(VFC_HTOPS(sequenceCtrl)), 2);
	memcpy(pdu + start + 4, &(VFC_HTOPS(step)), 2);
	return pdu + start + 6;
}

u8* Verify_ExecutionProcessFailure(u16 packetId, u16 sequenceCtrl, u16 step, u8 code, u8 param, u8* pdu, int start)
{
	memcpy(pdu + start, &(VFC_HTOPS(packetId)), 2);
	memcpy(pdu + start + 2, &(VFC_HTOPS(sequenceCtrl)), 2);
	memcpy(pdu + start + 4, &(VFC_HTOPS(step)), 2);
	*(pdu + start + 6) = code;
	*(pdu + start + 7) = param;
	return pdu + start + 8;
}

u8* Verify_ExecutionComplete(u16 packetId, u16 sequenceCtrl, u8* pdu, int start)
{
	memcpy(pdu + start, &(VFC_HTOPS(packetId)), 2);
	memcpy(pdu + start + 2, &(VFC_HTOPS(sequenceCtrl)), 2);
	return pdu + start + 4;
}

u8* Verify_ExecutionCompleteFailure(u16 packetId, u16 sequenceCtrl, u8 code, u8 param, u8* pdu, int start)
{
	memcpy(pdu + start, &(VFC_HTOPS(packetId)), 2);
	memcpy(pdu + start + 2, &(VFC_HTOPS(sequenceCtrl)), 2);
	*(pdu + start + 4) = code;
	*(pdu + start + 5) = param;
	return pdu + start + 6;
}

/*
 * housekeeping.h
 *
 *  Created on: 03.02.2014
 *      Author: Rayne
 */

#ifndef HOUSEKEEPING_H_
#define HOUSEKEEPING_H_

/***************************** Include Files *********************************/
#include <xil_types.h>
#include "pus_config.h"
#include "space_packet.h"
#include "pus.h"
#include "../contrib/hk2ddps.h"

/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
int Housekeeping_SendParameterReport(u16 src_apid, HK2DDPS_OrderType* orderPtr);
void Housekeeping_EnableReportHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer);
void Housekeeping_DisableReportHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer);
#endif /* HOUSEKEEPING_H_ */

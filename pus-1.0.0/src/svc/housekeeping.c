/*
 * housekeeping.c
 *
 *  Created on: 03.02.2014
 *      Author: Rayne
 */

/***************************** Include Files *********************************/
#include "housekeeping.h"
#include "ack.h"
#include "pus_io.h"

/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
/************************ Function Implementations ***************************/
/**
 * @brief Sends housekeeping parameters report
 *
 * @param src_apid Source APID
 * @param orderPtr Order to use last sample for send.
 * @return Send status.
 * 	@arg #PUS_STATUS_OK - Send success.
 */
int Housekeeping_SendParameterReport(u16 src_apid, HK2DDPS_OrderType* orderPtr)
{
	u8* buffer = PUS_PacketOpenTelemetry(src_apid, 3, 25);
	u8* begin = buffer;

	//TODO:Check available buffer size.

	int wrote_w = HK2DDPS_READ_LAST_SAMPLE(orderPtr, (u16*)buffer);

	buffer += (HK2DDPS_DATA_POOL_SAMPLE_PARAMETERS_SIZE_WORDS(orderPtr) << 1);

	PUS_PacketRelease(buffer - begin);
	return PUS_STATUS_OK;
}

/**
 * @brief Enables housekeeping report
 *
 * @param pus_pcb Current pus pcb.
 * @param sp_pcb Current sp pcb.
 * @param buffer Buffer with incoming data.
 */
void Housekeeping_EnableReportHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer)
{
	int status = ACK_SendAccept(SP_CurrentHeader, pus_pcb->PUS_TCHeader);
	if(status != PUS_STATUS_OK)
		return;

	u16 sid = 0;
	buffer = PUS_Read16(buffer, &sid);

	HK2DDPS_ENABLE_REPORT(sid);

	ACK_SendExecutionComplete(SP_CurrentHeader, pus_pcb->PUS_TCHeader);

}

/**
 * @brief Disables housekeeping report
 *
 * @param pus_pcb Current pus pcb.
 * @param sp_pcb Current sp pcb.
 * @param buffer Buffer with incoming data.
 */
void Housekeeping_DisableReportHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer)
{
	int status = ACK_SendAccept(SP_CurrentHeader, pus_pcb->PUS_TCHeader);
	if(status != PUS_STATUS_OK)
		return;

	u16 sid = 0;
	buffer = PUS_Read16(buffer, &sid);

	HK2DDPS_DISABLE_REPORT(sid);

	ACK_SendExecutionComplete(SP_CurrentHeader, pus_pcb->PUS_TCHeader);
}

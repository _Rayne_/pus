/**
 * @file trace.h
 *
 * @brief Brief description.
 *
 * Created by Rayne on 09.09.2014
 * Version 1.0.0.0
 */

#ifndef TRACE_H_
#define TRACE_H_

/***************************** Include Files *********************************/
/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
/* C syntax  void TRACE_WRITE(const char* message) */
#define TRACE_WRITE(message)

/* C syntax  void TRACE_WRITE_F(const char* format, ...) */
#define TRACE_WRITE_F(format, ...)

/* C syntax  void TRACE_WRITE_CF(const char* category, const char* format, ...) */
#define TRACE_WRITE_CF(category, format, ...)


/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/


#endif /* TRACE_H_ */

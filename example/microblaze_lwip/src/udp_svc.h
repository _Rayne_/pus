/*
 * udp_svc.h
 *
 *  Created on: 20.11.2013
 *      Author: Rayne
 */

#ifndef UDP_SVC_H_
#define UDP_SVC_H_

#include <xil_types.h>
#include "lwip/err.h"
#include "lwip/udp.h"

#define UDP_RX_PORT			501
#define UDP_TX_PORT			501

typedef int (*udp_svc_recv_callback)(struct ip_addr src_addr, u16_t src_port, struct ip_addr dest_addr, u16_t dest_port, u8* data, int len);

void start_udp();
void udp_svc_recv(udp_svc_recv_callback callback);
void udp_svc_send_to(struct ip_addr* dest_ip, u16_t port, u8* data, int len);
void udp_svc_send(u8* data, int len);

#endif /* UDP_SVC_H_ */

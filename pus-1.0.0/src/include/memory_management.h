/*
 * memory_management.h
 *
 *  Created on: 19.12.2013
 *      Author: Rayne
 */

#ifndef MEMORY_MANAGEMENT_H_
#define MEMORY_MANAGEMENT_H_

/***************************** Include Files *********************************/
#include <xil_types.h>
#include "pus_config.h"
#include "pus.h"
#include "space_packet.h"

/************************** Constant Definitions *****************************/
#define MM_BLOCK_SIZE_MAX_BYTES				100000
#define MM_BLOCKS_MAX_NUM					10
#define MM_SMALLEST_ADDRESSABLE_UNIT		SMALLEST_ADDRESSABLE_UNIT
#define MM_MEMORY_NAME_LENGTH				10

/**************************** Type Definitions *******************************/
#if MM_SMALLEST_ADDRESSABLE_UNIT == 8
typedef u8 MM_SAU;
#elif MM_SMALLEST_ADDRESSABLE_UNIT == 16
typedef u16 MM_SAU;
#elif MM_SMALLEST_ADDRESSABLE_UNIT == 32
typedef u32 MM_SAU;
#endif

typedef struct {
	union{
		s32 Offset;
		u32 StartAddr;
	}Field1;
	u32 Length;
}Memory_Block;

/***************** Macros (Inline Functions) Definitions *********************/
#if MM_SMALLEST_ADDRESSABLE_UNIT == 8
#define MM_SDU_BYTES(length)			(length)
#define MM_SDU_TO_16_WORDS(length)		((length) >> 1)
#elif MM_SMALLEST_ADDRESSABLE_UNIT == 16
#define MM_SDU_TO_BYTES(length)			((length) << 1)
#define MM_SDU_TO_16_WORDS(length)		(length)
#elif MM_SMALLEST_ADDRESSABLE_UNIT == 32
#define MM_SDU_BYTES(length)			((length) << 2)
#define MM_SDU_TO_16_WORDS(length)		((length) << 1)
#endif


/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
void Memory_LoadDataBaseOffsetHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer);
void Memory_DumpDataBaseOffsetHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer);
void Memory_CheckDataBaseOffsetHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer);
void Memory_LoadDataAbsoluteHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer);
void Memory_DumpDataAbsoluteHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer);
void Memory_CheckDataAbsoluteHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer);
#endif /* MEMORY_MANAGEMENT_H_ */

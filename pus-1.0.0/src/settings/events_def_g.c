/**
 * @file events_def_g.c
 *
 * @brief Events definition.
 *
 * Global table used across all application.
 *
 * Created by Rayne on 03.09.2014
 * Version 1.0.0.0
 */

/***************************** Include Files *********************************/
#include "../include/event_reporting.h"

/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/


/************************** Variable Definitions *****************************/
EVENT_REPORTS_TABLE_BEGIN()
//EVENT_REPORTS_TABLE_ENTRY(0, TRUE),
EVENT_REPORTS_TABLE_END();

/************************** Function Prototypes ******************************/
/************************ Function Implementations ***************************/



/*
 * storage_retrieval.c
 *
 *  Created on: 15.04.2014
 *      Author: Rayne
 */

/***************************** Include Files *********************************/
#include "storage_retrieval.h"
#include "ack.h"
#include "pus_io.h"
#include "../contrib/mm2fs.h"
#include "../contrib/trace.h"

/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
/************************** Variable Definitions *****************************/
//Packets storage fills each time from the beginning.
int Storage_CurrentOffset = 0;

u8 packetBuff[10000];
/************************** Function Prototypes ******************************/
/************************ Function Implementations ***************************/
void Storage_Input(u8* data, int len)
{
	int f = MM_FILE_OPEN("STORAGE");
	if(f == -1)
	{
		TRACE_WRITE_CF("ERROR", "Could not open packets storage.\r\n");
		return;
	}

	int wrote = MM_FILE_WRITE(f, Storage_CurrentOffset, (u16*)data, len >> 1);

	if(wrote == 0)
		TRACE_WRITE_CF("ERROR", "Could not write packets storage.\r\n");

	if(wrote < (len >> 1))
		TRACE_WRITE_CF("WARNING", "Packets storage overflow.\r\n");


	Storage_CurrentOffset += (wrote << 1);
	if(Storage_CurrentOffset >= STORAGE_SIZE)
	{
		Storage_CurrentOffset = 0;
	}

	MM_FILE_CLOSE(f);
}

/**
 * @brief Downlink all packets that was stored in storage.
 *
 * Searches packets in packets storage and send them to dedicated virtual channel.
 * Searching stops when all packets was sent or zero packet reached(packet with Length == 0).
 */
void Storage_DownlinkAllHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer)
{
	int status = PUS_STATUS_OK;

	/* Send acceptance */
	status = ACK_SendAccept(SP_CurrentHeader, pus_pcb->PUS_TCHeader);
	if(status != PUS_STATUS_OK)
		return;

	int overwrite_ptr = Storage_CurrentOffset;
	int downlink_ptr = overwrite_ptr;
	int f = MM_FILE_OPEN("STORAGE");

	if(f != -1)
	{
		while(1){
			//Read packet header.
			int wread = MM_FILE_READ(f, downlink_ptr, (u16*)packetBuff, SP_HEADER_SIZE_BYTES >> 1);

			//If read successful go  further
			if(wread == (SP_HEADER_SIZE_BYTES >> 1))
			{
				//Get packet length.
				SP_PacketHeader header;
				SP_HeaderFromProtocolBytes((u8*)packetBuff, 0, &header);

				//Exit when find packet with length == 0, if it is the first packet
				//what we have read then reset storage to beginning.
				if(header.PacketLength == 0){
					if(downlink_ptr != overwrite_ptr)
						break;
					else
					{
						downlink_ptr = 0;
						continue;
					}
				}

				//Read all packet including header again.
				int packet_tot_len_bytes = SP_HEADER_SIZE_BYTES + header.PacketLength + 1;
				wread = MM_FILE_READ(f, downlink_ptr, (u16*)packetBuff, packet_tot_len_bytes >> 1);

				//Send packet if it is was read successfully.
				if(wread == (packet_tot_len_bytes >> 1))
				{
					int status = NET_PACKET_OUT(0, 0, 0, (u8*)packetBuff, packet_tot_len_bytes);
					if(status != 0)
						TRACE_WRITE_CF("ERROR", "Packet send error %d\r\n", status);

				}else if(wread == 0)
				{
					TRACE_WRITE_CF("ERROR", "Reading storage error\r\n");
					break;
				}
			}else if(wread == 0)
			{
				TRACE_WRITE_CF("ERROR", "Reading storage error\r\n");
				break;
			}

			//Increment storage downlink pointer.
			downlink_ptr += (wread << 1);
			if(downlink_ptr >= STORAGE_SIZE)
				downlink_ptr = 0;

			//When we reached overwrite pointer then all packets had been downlinked.
			if(downlink_ptr == overwrite_ptr)
				break;
		}
		MM_FILE_CLOSE(f);
	}else
	{
		TRACE_WRITE_CF("ERROR", "Could not open packets storage.\r\n");
	}

	ACK_SendExecutionComplete(SP_CurrentHeader, pus_pcb->PUS_TCHeader);
}

/**
 * @brief Downlink packet within specified time period.
 *
 */
void Storage_DownlinkTimePeriodHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer)
{
//	int status = PUS_STATUS_OK;

	ACK_SendFailure(SP_CurrentHeader, pus_pcb->PUS_TCHeader, VFC_APPLICATION_ERROR, 0);

//	status = ACK_SendAccept(SP_CurrentHeader, pus_pcb->PUS_TCHeader);
//	if(status != PUS_STATUS_OK)
//		return;
//
//	//TODO: Add Storage_DownlinkTimePeriodHandler packet handling here
//
//	ACK_SendExecutionComplete(SP_CurrentHeader, pus_pcb->PUS_TCHeader);
}


/**
 * @file pus_status.h
 *
 * @brief Contains pus statuses.
 *
 * Created by Rayne on 06.06.2014
 * Version 1.0.0.0
 */

#ifndef PUS_STATUS_H_
#define PUS_STATUS_H_

/***************************** Include Files *********************************/
/************************** Constant Definitions *****************************/
#define PUS_STATUS_OK								0
#define PUS_STATUS_ERROR							1
#define PUS_STATUS_SP_PROTOCOL_ERROR				2
#define PUS_STATUS_PUS_PROTOCOL_ERROR				3
#define PUS_STATUS_PACKET_SVC_ERROR					4
#define PUS_STATUS_NO_HANDLER_ERROR					5
#define PUS_STATUS_DAS_ERROR						6

#define PUS_STATUS_MM_INVALID_BLOCKS_NUM			7
#define PUS_STATUS_MM_INVALID_MEMORY_NAME			8

#define PUS_STATUS_INVALID_EVENT_ID					9
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/


#endif /* PUS_STATUS_H_ */

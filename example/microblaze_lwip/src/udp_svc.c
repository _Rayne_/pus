/*
 * udp_svc.c
 *
 *  Created on: 20.11.2013
 *      Author: Rayne
 */

#include "udp_svc.h"

#include <stdio.h>
#include <string.h>




static struct udp_pcb * pcb = NULL;
static struct ip_addr addr_last;
static u16_t port_last = 0;

static udp_svc_recv_callback recv_callback = NULL;

void udp_recv_handler(void *arg, struct udp_pcb *pcb, struct pbuf *p, struct ip_addr *addr, u16_t port);


void start_udp(){
	pcb = udp_new();

	udp_bind(pcb, IP_ADDR_ANY, UDP_RX_PORT);

	udp_recv(pcb, udp_recv_handler, NULL);
}

void udp_svc_recv(udp_svc_recv_callback callback)
{
	recv_callback = callback;
}

void udp_recv_handler(void *arg, struct udp_pcb *pcb, struct pbuf *p, struct ip_addr *addr, u16_t port){
	xil_printf("received at %d, echoing to the same port\r\n",pcb->local_port);
	//dst_ip = &(pcb->remote_ip); // this is zero always
	if (p != NULL) {

		/* save incoming data before call recv callback */
		addr_last = *addr;
		port_last = port;

		xil_printf("UDP rcv %d bytes\r\n", (*p).len);

		if(recv_callback == NULL)
		{
		  xil_printf("UDP no callback registered.\r\n");
		  return;
		}

		int err = recv_callback(addr_last, port_last, pcb->local_ip,pcb->local_port, (u8*)(p->payload), p->len);

		if (err)
		  xil_printf("Protocol error : %d\r\n", err);

		pbuf_free(p);
	}
}

void udp_svc_send_to(struct ip_addr* dest_ip, u16_t port, u8* data, int len)
{
	if(pcb != NULL){
		struct pbuf * pb = pbuf_alloc(PBUF_TRANSPORT, len, PBUF_REF);
		pb->payload = data;
		pb->len = pb->tot_len = len;
		err_t err = udp_sendto(pcb, pb, dest_ip, port);
//		err_t err = udp_sendto(pcb, pb, &addr_last, UDP_TX_PORT);
		xil_printf("UDP sent %d bytes\r\n", len);
		if(err != ERR_OK)
			xil_printf("UDP send error %d\r\n", err);

		pbuf_free(pb);
	}
	//TODO: Should i free pbuf?
}

void udp_svc_send(u8* data, int len)
{
	udp_svc_send_to(&addr_last, port_last, data, len);

//	if(pcb != NULL){
//		struct pbuf * pb = pbuf_alloc(PBUF_TRANSPORT, len, PBUF_REF);
//		pb->payload = data;
//		pb->len = pb->tot_len = len;
//		err_t err = udp_sendto(pcb, pb, &addr_last, port_last);
////		err_t err = udp_sendto(pcb, pb, &addr_last, UDP_TX_PORT);
//		xil_printf("UDP sent %d bytes\r\n", len);
//		if(err != ERR_OK)
//			xil_printf("UDP send error %d\r\n", err);
//
//		pbuf_free(pb);
//	}
//	//TODO: Should i free pbuf?
}

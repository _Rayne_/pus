/**
 * @file pus_config.h
 *
 * @brief Brief description.
 *
 * Created by Rayne on 20.06.2014
 * Version 1.0.0.0
 */

#ifndef PUS_CONFIG_H_
#define PUS_CONFIG_H_

/***************************** Include Files *********************************/
/************************** Constant Definitions *****************************/
#define PUS_HTOPS(val)		(val)
#define PUS_PTOHS(val)		(val)
#define PUS_HTOPL(val)		(val)
#define PUS_PTOHL(val)		(val)

#define _PUS_DEBUG_

/** @name Mission constants @{ */
#define SMALLEST_ADDRESSABLE_UNIT		16
//@}

#define DISABLED						0
#define MIN_CAP							1
#define ADD_CAP							2

/**
 * @name PUS application configuration.
 * @{
 */
#define CURRENT_APID					3					//Current application process id.
#define EVENT_REPORTIN_SVC				(MIN_CAP | ADD_CAP)	//Defines event reporting service capability (5).
#define MEMORY_MANAGEMENT_SVC			MIN_CAP				//Defines memory management service capability (6).
#define STORAGE_RETRIEVAL_SVC			MIN_CAP				//Defines storage and retrieval service capability (15).
#define TEST_SVC						MIN_CAP				//Defines test service capability (17).
//@}
//TODO: Add autoinitialisation.
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/


#endif /* PUS_CONFIG_H_ */

/**
 * @file mm2fs.h
 *
 * @brief Mapping mm to fs.
 *
 * Created by Rayne on 03.09.2014
 * Version 1.0.0.0
 */

#ifndef MM2FS_H_
#define MM2FS_H_

/***************************** Include Files *********************************/
/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
//TODO: You should implement memory access logic
#define MM_MEM_OPEN(name)								-1

#define MM_MEM_CLOSE(handle)							-1

#define MM_MEM_READ(handle, base, offset, dest, wlen)	0

#define MM_MEM_WRITE(handle, base, offset, src, wlen)	0

#define MM_MEM_CHECK(handle, base, offset, wlen)		0



#define MM_FILE_OPEN(name)								-1

#define MM_FILE_CLOSE(handle)							-1

#define MM_FILE_READ(handle, offset, dest, wlen)		0

#define MM_FILE_WRITE(handle, offset, src, wlen)		0

#define MM_FILE_CHECK(handle, base, offset, wlen)		0
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/


#endif /* MM2FS_H_ */

/**
 * @file pus.h
 *
 * @brief
 *  Created on: 05.12.2013
 *      Author: Rayne
 * */

#ifndef PUS_H_
#define PUS_H_

/***************************** Include Files *********************************/
#include <xil_types.h>
#include "space_packet.h"
#include "pus_status.h"
#include "pus_config.h"

/************************** Constant Definitions *****************************/
#define PUS_VERSION						0

#define PUS_TC_HEADER_SIZE				4
#define PUS_TM_HEADER_SIZE				12
#define PUS_TM_CHECKSUM_SIZE			2

/**
 * @name ACK Flags.
 *
 * @{
 */
#define PUS_ACK_ACCEPTANCE				0x01	///< Acceptance
#define PUS_ACK_EXECUTION_START			0x02	///< Execution start
#define PUS_ACK_EXECUTION_PROGRESS		0x04	///< Execution progress
#define PUS_ACK_EXECUTION_COMPLETION	0x08	///< Execution completion
// @}
/**************************** Type Definitions *******************************/
struct PUS_PCB;

/**
 * @brief PUS handler.
 *
 * Represents PUS application handler. Handler is calling for each registered SAP access transaction.
 * Handler is provided with packet id, sequence controller and application data pointer in packet data field.
 *
 * @param packet_id Space packet identity.
 * @param sequence_ctrl Space packet sequence controller.
 * @param buffer Application data pointer.
 * @param offset Application data offset.
 */
typedef void (*PUS_ReceiveHandler)(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer);

/**
 * @brief Basic PUS packet header.
 */
typedef struct __attribute__ ((__packed__)){
	u8 FlagVerACK;			///< ACK
	u8 ServiceType;			///< Service type
	u8 ServiceSubType;		///< Service sub type
}PUS_PacketHeaderBase;

/**
 * @brief PUS telecommand packet header.
 */
typedef struct __attribute__ ((__packed__)){
	PUS_PacketHeaderBase Base;	///< Base part
	u8 SourceId;				///< Source identifier
}PUS_TCPacketHeader;

/**
 * @brief PUS telemetry packet header.
 */
typedef struct __attribute__ ((__packed__)){
	PUS_PacketHeaderBase Base;	///< Base part
	u8 Counter;					///< TM Packets counter
	u64 Time;					///< Time reference
}PUS_TMPacketHeader;

struct PUS_PCB
{
	struct PUS_PCB* Next;

	u16 APID;

	u8 ServiceType;			///< Service type
	u8 ServiceSubType;		///< Service sub type

	u8 Counter;				///< TM Packets counter, valid only for telecommands.

	PUS_TCPacketHeader PUS_TCHeader;	///< Received packet header buffer.
	PUS_TMPacketHeader PUS_TMHeader;	///< Transmitted packet header buffer.

	PUS_ReceiveHandler RecvHandler;		///< Packet handler.
};

/***************** Macros (Inline Functions) Definitions *********************/
#define PUS_HEADER_BASE_TC_GET_CCSDSSHF(flag_ver_ack)				(((flag_ver_ack) & 0x80) >> 7)
#define PUS_HEADER_BASE_TC_SET_CCSDSSHF(flag_ver_ack, value)		((flag_ver_ack) = ((flag_ver_ack) & ~0x80) | (((value) << 7)  & 0x80))

#define PUS_HEADER_BASE_GET_VERSION(flag_ver_ack)					(((flag_ver_ack) & 0x70) >> 4)
#define PUS_HEADER_BASE_SET_VERSION(flag_ver_ack, value)			((flag_ver_ack) = ((flag_ver_ack) & ~0x70) | (((value) << 4)  & 0x70))

#define PUS_HEADER_BASE_TC_GET_ACK(flag_ver_ack)					((flag_ver_ack) & 0x0F)
#define PUS_HEADER_BASE_TC_SET_ACK(flag_ver_ack, value)				((flag_ver_ack) = ((flag_ver_ack) & ~0x0F) | ((value)  & 0x0F))

#define PUS_HEADER_BASE_TC_CHECK_ACK(flag_ver_ack, ack)				(PUS_HEADER_BASE_TC_GET_ACK(flag_ver_ack) & (ack))

#ifdef _PUS_DEBUG_
#define PUS_DEBUG_PRINT(...) xil_printf( __VA_ARGS__ )
#else
#define PUS_DEBUG_PRINT(...)
#endif

#define SP_HTOPL(val)		((val >> 24) | ((val >> 8) & 0x0000FF00) | ((val << 8) & 0x00FF0000) | ((val << 24) & 0xFF000000))
#define SP_PTOHL(val)		((val >> 24) | ((val >> 8) & 0x0000FF00) | ((val << 8) & 0x00FF0000) | ((val << 24) & 0xFF000000))
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
struct PUS_PCB* PUS_PCBNew();
void PUS_Add(struct PUS_PCB* pcb, u16 apid, u8 svcType, u8 svcSubType, PUS_ReceiveHandler handler);
void PUS_Input(u8* buffer, const struct SP_PCB* sp_pcb);
u8* PUS_PacketOpenCommand(u16 destApid, u8 destSvcType, u8 destSvcSubType, u8 ack, u8 sourceId);
u8* PUS_PacketOpenTelemetry(u16 srcApid, u8 srcSvcType, u8 srcSvcSubType);
void PUS_PacketRelease(int appDataLen);
u16 PUS_Fletcher16( u8 const *data, size_t bytes );
void PUS_PrintHeader(PUS_PacketHeaderBase header);

//u8* PUS_TCRead(PUS_ServiceProvider* instancePtr, u8* pdu, int pdu_start, int* sap);
//u8* PUS_TMWrite(PUS_ServiceProvider* instancePtr, int sap, u8* pdu, int pdu_start);
//u8* PUS_TMWriteChecksum(PUS_ServiceProvider* instancePtr, u8* pdu, int pdu_start, u8* position);
//u8 PUS_CheckTCPCUACK(PUS_ServiceProvider* instancePtr, u8 flag);

#endif /* PUS_H_ */

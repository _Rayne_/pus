/*
 * test.h
 *
 *  Created on: 23.01.2014
 *      Author: Rayne
 */

#ifndef TEST_H_
#define TEST_H_

/***************************** Include Files *********************************/
#include "pus.h"

/************************** Constant Definitions *****************************/

#define TEST_OK					0
#define TEST_ERROR				1

/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
void Test_ConnectionHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer);

#endif /* TEST_H_ */

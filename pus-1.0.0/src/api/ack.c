/**
 * @file ack.c
 *
 * @brief Brief description.
 *
 * Created by Rayne on 05.06.2014
 * Version 1.0.0.0
 */

/***************************** Include Files *********************************/
#include "ack.h"


/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
/************************ Function Implementations ***************************/
/**
 * @brief Sends accept notification if corresponding ACK flag selected.
 *
 * @param procPktSPHeader SP header of accepting packet.
 * @param procPktPUSHeader PUS header of accepting packet.
 * @return Send status
 * 	@arg PUS_STATUS_OK Send succeed.
 * 	@arg PUS_STATUS_ERROR Send failed.
 */
int ACK_SendAccept(SP_PacketHeader procPktSPHeader, PUS_TCPacketHeader procPktPUSHeader)
{
	if(!PUS_HEADER_BASE_TC_CHECK_ACK(procPktPUSHeader.Base.FlagVerACK, PUS_ACK_ACCEPTANCE))
		return PUS_STATUS_OK;

	u8* buff = PUS_PacketOpenTelemetry(SP_PACKET_ID_GET_APID(procPktSPHeader.PacketId), 1, 1);
	if(buff == NULL)
		return PUS_STATUS_ERROR;

	u8* buffEnd = Verify_Accept(procPktSPHeader.PacketId, procPktSPHeader.PacketSequenceControl, buff, 0);
	PUS_PacketRelease(buffEnd - buff);

	return PUS_STATUS_OK;
}
int ACK_SendFailure(SP_PacketHeader procPktSPHeader, PUS_TCPacketHeader procPktPUSHeader, Verify_FailureCode code, u8 param)
{
//	if(!PUS_HEADER_BASE_TC_CHECK_ACK(procPktPUSHeader.Base.FlagVerACK, PUS_ACK_ACCEPTANCE))
//		return PUS_STATUS_OK;

	u8* buff = PUS_PacketOpenTelemetry(SP_PACKET_ID_GET_APID(procPktSPHeader.PacketId), 1, 2);
	if(buff == NULL)
		return PUS_STATUS_ERROR;

	u8* buffEnd = Verify_Failure(procPktSPHeader.PacketId, procPktSPHeader.PacketSequenceControl, code, param, buff, 0);
	PUS_PacketRelease(buffEnd - buff);

	return PUS_STATUS_OK;
}
int ACK_SendExecutionStart(SP_PacketHeader procPktSPHeader, PUS_TCPacketHeader procPktPUSHeader){
	if(!PUS_HEADER_BASE_TC_CHECK_ACK(procPktPUSHeader.Base.FlagVerACK, PUS_ACK_EXECUTION_START))
		return PUS_STATUS_OK;

	u8* buff = PUS_PacketOpenTelemetry(SP_PACKET_ID_GET_APID(procPktSPHeader.PacketId), 1, 3);
	if(buff == NULL)
		return PUS_STATUS_ERROR;

	u8* buffEnd = Verify_ExecutionStart(procPktSPHeader.PacketId, procPktSPHeader.PacketSequenceControl, buff, 0);
	PUS_PacketRelease(buffEnd - buff);

	return PUS_STATUS_OK;
}
int ACK_SendExecutionStartFailure(SP_PacketHeader procPktSPHeader, PUS_TCPacketHeader procPktPUSHeader, u8 code, u8 param){
//	if(!PUS_HEADER_BASE_TC_CHECK_ACK(procPktPUSHeader.Base.FlagVerACK, PUS_ACK_EXECUTION_START))
//		return PUS_STATUS_OK;

	u8* buff = PUS_PacketOpenTelemetry(SP_PACKET_ID_GET_APID(procPktSPHeader.PacketId), 1, 4);
	if(buff == NULL)
		return PUS_STATUS_ERROR;

	u8* buffEnd = Verify_ExecutionStartFailure(procPktSPHeader.PacketId, procPktSPHeader.PacketSequenceControl, code, param, buff, 0);
	PUS_PacketRelease(buffEnd - buff);

	return PUS_STATUS_OK;
}
int ACK_SendExecutionProcess(SP_PacketHeader procPktSPHeader, PUS_TCPacketHeader procPktPUSHeader, u16 step){
	if(!PUS_HEADER_BASE_TC_CHECK_ACK(procPktPUSHeader.Base.FlagVerACK, PUS_ACK_EXECUTION_PROGRESS))
		return PUS_STATUS_OK;

	u8* buff = PUS_PacketOpenTelemetry(SP_PACKET_ID_GET_APID(procPktSPHeader.PacketId), 1, 5);
	if(buff == NULL)
		return PUS_STATUS_ERROR;

	u8* buffEnd = Verify_ExecutionProcess(procPktSPHeader.PacketId, procPktSPHeader.PacketSequenceControl, step, buff, 0);
	PUS_PacketRelease(buffEnd - buff);

	return PUS_STATUS_OK;
}
int ACK_SendExecutionProcessFailure(SP_PacketHeader procPktSPHeader, PUS_TCPacketHeader procPktPUSHeader, u16 step, u8 code, u8 param){
//	if(!PUS_HEADER_BASE_TC_CHECK_ACK(procPktPUSHeader.Base.FlagVerACK, PUS_ACK_EXECUTION_PROGRESS))
//		return PUS_STATUS_OK;

	u8* buff = PUS_PacketOpenTelemetry(SP_PACKET_ID_GET_APID(procPktSPHeader.PacketId), 1, 6);
	if(buff == NULL)
		return PUS_STATUS_ERROR;

	u8* buffEnd = Verify_ExecutionProcessFailure(procPktSPHeader.PacketId, procPktSPHeader.PacketSequenceControl, step, code, param, buff, 0);
	PUS_PacketRelease(buffEnd - buff);

	return PUS_STATUS_OK;
}

int ACK_SendExecutionComplete(SP_PacketHeader procPktSPHeader, PUS_TCPacketHeader procPktPUSHeader){
	if(!PUS_HEADER_BASE_TC_CHECK_ACK(procPktPUSHeader.Base.FlagVerACK, PUS_ACK_EXECUTION_COMPLETION))
		return PUS_STATUS_OK;

	u8* buff = PUS_PacketOpenTelemetry(SP_PACKET_ID_GET_APID(procPktSPHeader.PacketId), 1, 7);
	if(buff == NULL)
		return PUS_STATUS_ERROR;

	u8* buffEnd = Verify_ExecutionComplete(procPktSPHeader.PacketId, procPktSPHeader.PacketSequenceControl, buff, 0);
	PUS_PacketRelease(buffEnd - buff);

	return PUS_STATUS_OK;
}

int ACK_SendExecutionCompleteFailure(SP_PacketHeader procPktSPHeader, PUS_TCPacketHeader procPktPUSHeader, u8 code, u8 param){
//	if(!PUS_HEADER_BASE_TC_CHECK_ACK(procPktPUSHeader.Base.FlagVerACK, PUS_ACK_EXECUTION_COMPLETION))
//		return PUS_STATUS_OK;

	u8* buff = PUS_PacketOpenTelemetry(SP_PACKET_ID_GET_APID(procPktSPHeader.PacketId), 1, 8);
	if(buff == NULL)
		return PUS_STATUS_ERROR;

	u8* buffEnd = Verify_ExecutionCompleteFailure(procPktSPHeader.PacketId, procPktSPHeader.PacketSequenceControl, code, param, buff, 0);
	PUS_PacketRelease(buffEnd - buff);

	return PUS_STATUS_OK;
}


/*
 * Copyright (c) 2009 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/*
 * helloworld.c: simple test application
 */

/* Includes */
#include "xmk.h"
#include "os_config.h"
#include "sys/ksched.h"
#include "sys/init.h"
#include "config/config_param.h"
#include "stdio.h"
#include "xparameters.h"
#include "platform.h"
#include "platform_config.h"
#include <pthread.h>
#include <sys/types.h>
#include "lwip/udp.h"
#include "netif/xadapter.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/init.h"
#include "config_apps.h"

#include "udp_svc.h"

#include "pus.h"
#include "ack.h"

#include "test.h"

#define PLATFORM_EMAC_BASEADDR	XPAR_EMACLITE_0_BASEADDR

struct netif server_netif;

int udp_svc_recv_cb(struct ip_addr src_addr, u16_t src_port, struct ip_addr dest_addr, u16_t dest_port, u8* data, int len);
void pus_17_1_handler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer);

int main()
{
    xilkernel_main();

    /* Never reached */
    cleanup_platform();

    return 0;
}

int udp_svc_recv_cb(struct ip_addr src_addr, u16_t src_port, struct ip_addr dest_addr, u16_t dest_port, u8* data, int len)
{
	xil_printf("Udp input\r\n");
	SP_Input(data);
	return 0;
}

void pus_17_1_handler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer)
{
	xil_printf("PUS input\r\n");
	ACK_SendAccept(SP_CurrentHeader, pus_pcb->PUS_TCHeader);
	ACK_SendExecutionStart(SP_CurrentHeader, pus_pcb->PUS_TCHeader);
	ACK_SendExecutionProcess(SP_CurrentHeader, pus_pcb->PUS_TCHeader, 1);
	ACK_SendExecutionComplete(SP_CurrentHeader, pus_pcb->PUS_TCHeader);

}

void launch_app_threads()
{
	xil_printf("launch_app_threads\r\n");

	udp_svc_recv(udp_svc_recv_cb);
	start_udp();

	struct SP_PCB * sp = SP_PCBNew();
	SP_Add(sp, 3);

	struct PUS_PCB * pp = PUS_PCBNew();
	PUS_Add(pp, 3, 17,1, Test_ConnectionHandler);

//    sys_thread_new("mld_thread", (void(*)(void*))mld_thread, NULL,
//            THREAD_STACKSIZE,
//            DEFAULT_THREAD_PRIO);

//    sys_thread_new("udp_server_thread", (void(*)(void*))udp_server_thread, NULL,
//            THREAD_STACKSIZE,
//            DEFAULT_THREAD_PRIO);
}

void network_thread(void *p)
{
    struct netif *netif;
    struct ip_addr ipaddr, netmask, gw;

    /* the mac address of the board. this should be unique per board */
    unsigned char mac_ethernet_address[] = { 0x00, 0x0a, 0x35, 0x00, 0x01, 0x02 };

    netif = &server_netif;

    /* initliaze IP addresses to be used */
    IP4_ADDR(&ipaddr,  192, 168,   231, 10);
    IP4_ADDR(&netmask, 255, 255, 0,  0);
    IP4_ADDR(&gw,      192, 168,   1,  1);

    /* print out IP settings of the board */
//    print("\r\n\r\n");
//    print("-----lwIP Socket Mode Demo Application ------\r\n");
//    print_ip_settings(&ipaddr, &netmask, &gw);

    /* print all application headers */
//    print_headers();

    /* Add network interface to the netif_list, and set it as default */
    if (!xemac_add(netif, &ipaddr, &netmask, &gw, mac_ethernet_address, PLATFORM_EMAC_BASEADDR)) {
        xil_printf("Error adding N/W interface\r\n");
        return;
    }
    netif_set_default(netif);

    /* specify that the network if is up */
    netif_set_up(netif);

    /* start packet receive thread - required for lwIP operation */
    sys_thread_new("xemacif_input_thread", (void(*)(void*))xemacif_input_thread, netif,
            THREAD_STACKSIZE,
            DEFAULT_THREAD_PRIO);

    /* now we can start application threads */
    launch_app_threads();

    return;
}

int main_thread()
{
    /* initialize lwIP before calling sys_thread_new */
    lwip_init();

    /* any thread using lwIP should be created using sys_thread_new */
    sys_thread_new("NW_THREAD", network_thread, NULL,
            THREAD_STACKSIZE,
            DEFAULT_THREAD_PRIO);

    return 0;
}

/**
 * @file storage_selection.c
 *
 * @brief Brief description.
 *
 * Created by Rayne on 16.09.2014
 * Version 1.0.0.0
 */

/***************************** Include Files *********************************/
#include "storage_selection.h"
#include "storage_retrieval.h"
#include "ack.h"
#include "../contrib/trace.h"

/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
/************************** Variable Definitions *****************************/
/* Packets storage settings */
u8 Storage_SelectionEnabled = TRUE;

extern Storage_PacketSelection StoragePacketsFilter[];

//u8 Storage[STORAGE_SIZE] = {0};

//u8 packet[10000];

/************************** Function Prototypes ******************************/
/************************ Function Implementations ***************************/

/**
 * @brief Filter incoming packets.
 *
 * Do packets filtered saving into the storage.
 *
 * @param data Data source to send.
 * @param len The number of bytes to send.
 */
void Storage_SelectionInput(u8* data, int len)
{
	if((len & 0x1) == 0x1 || len == 0 || data == NULL)
	{
		TRACE_WRITE_CF("ERROR", "Could not save packet with even length.\r\n");
		return;
	}

	if(!Storage_SelectionEnabled)
		return;

	//Filter here.
	//Generated packet matched with current protocol control blocks.
	SP_PacketHeader header;
	SP_HeaderFromProtocolBytes(data, 0, &header);
	PUS_PacketHeaderBase pus_header = *((PUS_PacketHeaderBase*)(data + SP_HEADER_SIZE_BYTES));

	u16 apid = SP_PACKET_ID_GET_APID(header.PacketId);
	u8 st = pus_header.ServiceType;
	u8 sst = pus_header.ServiceSubType;

	int i = 0;
	for (i = 0; i < STORAGE_FILTER_SIZE; ++i) {

		//Zero APID means the end of filter.
		if(StoragePacketsFilter[i].APID == 0)
			return;

		if(StoragePacketsFilter[i].APID == apid &&
				StoragePacketsFilter[i].Type == st &&
				StoragePacketsFilter[i].SubType == sst)
		{
			//TODO:packet writing here.
			Storage_Input(data, len);
			return;
		}
	}
}

/**
 * @brief Enables packet selection sub-service.
 *
 * @param pus_pcb
 * @param sp_pcb
 * @param buffer
 */
void Storage_SelectionEnableHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer)
{
	int status = PUS_STATUS_OK;

	status = ACK_SendAccept(SP_CurrentHeader, pus_pcb->PUS_TCHeader);
	if(status != PUS_STATUS_OK)
		return;

	Storage_SelectionEnabled = TRUE;

	ACK_SendExecutionComplete(SP_CurrentHeader, pus_pcb->PUS_TCHeader);
}

/**
 * @brief Disables packet selection sub-service.
 *
 * @param pus_pcb
 * @param sp_pcb
 * @param buffer
 */
void Storage_SelectionDisableHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer)
{
	int status = PUS_STATUS_OK;

	status = ACK_SendAccept(SP_CurrentHeader, pus_pcb->PUS_TCHeader);
	if(status != PUS_STATUS_OK)
		return;

	Storage_SelectionEnabled = FALSE;

	ACK_SendExecutionComplete(SP_CurrentHeader, pus_pcb->PUS_TCHeader);
}

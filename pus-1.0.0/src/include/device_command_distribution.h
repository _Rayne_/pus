/**
 * @file device_command_distribution.h
 *
 * @brief Brief description.
 *
 * Created by Rayne on 27.05.2014
 * Version 1.0.0.0
 */

#ifndef DEVICE_COMMAND_DISTRIBUTION_H_
#define DEVICE_COMMAND_DISTRIBUTION_H_

/***************************** Include Files *********************************/
#include <xil_types.h>
#include "pus_config.h"

/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
void Device_DistributeCommandHandler(const struct PUS_PCB* pus_pcb, const struct SP_PCB * sp_pcb, u8* buffer);

#endif /* DEVICE_COMMAND_DISTRIBUTION_H_ */

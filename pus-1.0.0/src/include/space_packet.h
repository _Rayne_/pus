/**
 * @file space_packet_protocol.h
 *
 * @brief Space packet protocol definitions.
 *
 *  Created on: 21.11.2013
 *      Author: Rayne
 * */

#ifndef SPACE_PACKET_PROTOCOL_H_
#define SPACE_PACKET_PROTOCOL_H_

/***************************** Include Files *********************************/
#include <xil_types.h>
#include <stdlib.h>
#include "../contrib/net.h"


/************************** Constant Definitions *****************************/
#define _SP_DEBUG_

#define SP_VERSION 					0
#define SP_HEADER_SIZE_BYTES		6
//#define SP_DATA_FIELD_MAX_LEN		USHRT_MAX

#define SP_PACKET_ID_TC_TYPE		1
#define SP_PACKET_ID_TM_TYPE		0

/**
 * @name Protocol errors.
 * @{
 */
#define SP_OK						0
#define SP_ERROR					1
#define SP_INVALID_APID				2
#define SP_SEQUENCE_BREAK			3
#define SP_TYPE_MISSMATCH			4
#define SP_LENGTH_OVERFLOW			5
#define SP_SEQUENCE_ERROR			6
//@}

#define SP_PACK_CNT_MAX_VAL		0x3FFF

#define SP_MAX_LEN				1000

//#define SP_PACKET_GET_FIELD(field, mask)				((field) & mask)
//#define SP_PACKET_SET_FIELD(field, mask, value)			((field) = ((field) & ~mask) | ((value) & mask))

/**************************** Type Definitions *******************************/
typedef enum{
    Contineation = 0,
    First,
    Last,
    StandAlone
}SP_PacketSequenceFlag;

typedef struct __attribute__ ((__packed__)){
	u16 PacketId;
	u16 PacketSequenceControl;
	u16 PacketLength;
}SP_PacketHeader;

typedef struct {
	u16 PacketId;
	u16 PacketSequenceControl;
	void* CallBackRef;
}SP_LoockupTableEntry;

struct SP_PCB;

struct SP_PCB {
	struct SP_PCB* Next;

	u16 APID;

	u16 InputCounter;
	u16 OutputCounter;
//	SP_PacketHeader SP_RxHeader;
//	SP_PacketHeader SP_TxHeader;
	u8 Status;
}SP_PCB;

//typedef struct {
//	SP_PacketHeader header;
//	u8* payload;
//}SP_PacketBuf;

/***************** Macros (Inline Functions) Definitions *********************/
#define SP_HTOPS(val)		(((val) >> 8) | (((val) << 8) & 0xFF00))
#define SP_PTOHS(val)		(((val) >> 8) | (((val) << 8) & 0xFF00))

/**
 * @name Packet ID description.
 * @{
 */
#define SP_PACKET_ID_GET_VERSION(packetID)				(((packetID) & 0xE000) >> 13)
#define SP_PACKET_ID_SET_VERSION(packetID, ver)			((packetID) = ((packetID) & ~0xE000) | (((ver) << 13)  & 0xE000))

#define SP_PACKET_ID_GET_TYPE(packetID)					(((packetID) & 0x1000) >> 12)
#define SP_PACKET_ID_SET_TYPE(packetID, type)			((packetID) = ((packetID) & ~0x1000) | (((type) << 12)  & 0x1000))

#define SP_PACKET_ID_GET_HEADER_FLAG(packetID)			(((packetID) & 0x0800) >> 11)

/* C usage syntax SP_PACKET_ID_SET_HEADER_FLAG(u16, SP_PacketSequenceFlag) */
#define SP_PACKET_ID_SET_HEADER_FLAG(packetID, flag)	((packetID) = ((packetID) & ~0x0800) | (((flag) << 11)  & 0x0800))

#define SP_PACKET_ID_GET_APID(packetID)					((packetID) & 0x3FF)
#define SP_PACKET_ID_SET_APID(packetID, apid)			((packetID) = ((packetID) & ~0x3FF) | ((apid)  & 0x3FF))
#define SP_PACKET_ID_APID_CMP(IDleft, IDright)			(((IDleft ^ IDright) & 0x3FF) == 0)
///@}

/**
 * @name Packet sequence control description.
 * @{
 */
#define SP_PACKET_SEQ_CTRL_GET_FLAG(sequenceControl)				(((sequenceControl) & 0xC000) >> 14)
#define SP_PACKET_SEQ_CTRL_SET_FLAG(sequenceControl, flag)			((sequenceControl) = ((sequenceControl) & ~0xC000) | (((flag) << 14)  & 0xC000))

#define SP_PACKET_SEQ_CTRL_GET_CNT(sequenceControl)					(((sequenceControl) & 0x3FFF))
#define SP_PACKET_SEQ_CTRL_SET_CNT(sequenceControl, cnt)			((sequenceControl) = (((sequenceControl) & ~0x3FFF) | ((cnt)  & 0x3FFF)))
///@}

#ifdef _SP_DEBUG_
#define SP_DEBUG_PRINT(...) xil_printf( __VA_ARGS__ )
#else
#define SP_DEBUG_PRINT(...)
#endif

/************************** Variable Definitions *****************************/
extern SP_PacketHeader SP_CurrentHeader;
extern u8 TxBuffer[SP_MAX_LEN];

/************************** Function Prototypes ******************************/
struct SP_PCB* SP_PCBNew();

void SP_Add(struct SP_PCB* pcb, u16 apid);

void SP_Input(u8* buffer);

u8* SP_PacketOpen(u16 apid, u8 type, u8 secHeaderFlag, u8 seqFlag);

void SP_Release();

void SP_PacketSetSize(u16 size);
void SP_AppendPacketCS(int endOffset);
u16 SP_Fletcher16( u8 const *data, size_t bytes );

//int SP_SendCore(u32 pssap, u32 pdsap, u8 channel, u8* data, int len);

//void SP_Reset(struct SP_PCB* instancePtr);
void SP_PrintHeader(SP_PacketHeader header);

//u8* SP_HeaderWriteBegin(struct SP_PCB* instancePtr, int sap, u8* pdu, int pdu_start);
//int SP_HadetWriteEnd(struct SP_PCB* instancePtr, u8* pdu, int pdu_start, u16 data_field_length);

void SP_HeaderFromProtocolBytes(const u8* src, int start, SP_PacketHeader* header);
void SP_HeaderToProtocolBytes(const SP_PacketHeader* header, u8* dst, int start);
#endif /* SPACE_PACKET_PROTOCOL_H_ */
